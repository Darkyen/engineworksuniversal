package darkyenus.engineworksuniversals;

/**
 * Can be thrown by anything called in game loop. Indicates critical error that
 * can't be resolved. Can carry exception or message explaining the problem.
 *
 * @author Darkyen
 */
public class EngineworksError extends Error {

    public EngineworksError(Throwable cause) {
        super(cause);
    }

    public EngineworksError(String cause) {
        super(cause);
    }

    public EngineworksError(String comment,Throwable cause){
        super(comment,cause);
    }
}
