#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif
varying LOWP vec4 v_color;
varying vec2 v_texCoords;
uniform sampler2D u_texture;
uniform vec4 bounds;

void main()
{
    if(gl_FragCoord.x >= bounds.x && gl_FragCoord.x <= bounds.z && gl_FragCoord.y >= bounds.y && gl_FragCoord.y <= bounds.w){
        vec4 texColor = texture2D(u_texture, v_texCoords);
        gl_FragColor = texColor * v_color;
	}else{
	    discard;
	}
}