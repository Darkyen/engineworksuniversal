#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif
varying LOWP vec4 v_color;
varying vec2 v_texCoords;
uniform sampler2D u_texture;

uniform vec2 resolution;
uniform sampler2D maskTexture;
uniform int creationMode;

void main()
{
    if(creationMode == 1){
        //Writing to mask
        vec4 texColor = texture2D(u_texture, v_texCoords);
        gl_FragColor = texColor * v_color;
    } else {
        //Writing to image using mask
        vec4 color = texture2D(u_texture, v_texCoords) * v_color;
        vec4 maskColor = texture2D(maskTexture, gl_FragCoord.xy/resolution.xy);
        gl_FragColor = color * maskColor;
    }
}