#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif
varying LOWP vec4 v_color;
varying vec2 v_texCoords;
uniform sampler2D u_texture;
uniform vec2 maskOffset;

void main()
{
	vec4 texColor = texture2D(u_texture, v_texCoords);
	vec4 maskColor = texture2D(u_texture, v_texCoords + maskOffset);
	gl_FragColor.rgb = texColor.rgb * v_color.rgb;
	gl_FragColor.a = maskColor.a * v_color.a;
}