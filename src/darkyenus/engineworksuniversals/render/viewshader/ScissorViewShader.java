package darkyenus.engineworksuniversals.render.viewshader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import darkyenus.engineworksuniversals.geometry.Rectangle;
import darkyenus.engineworksuniversals.render.View;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/5/13
 * Time: 5:03 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class ScissorViewShader implements ViewShader {

    private static final String BOUNDS = "bounds";
    private Rectangle scissor;
    private boolean scissorChanged = false;

    @Override
    public void size(int width, int height) {
    }

    @Override
    public ShaderProgram createShaderProgram() {
        return View.createShader(Gdx.files.internal("darkyenus/engineworksuniversals/render/viewshader/shaderfiles/default.vert"), Gdx.files.internal("darkyenus/engineworksuniversals/render/viewshader/shaderfiles/scissor.frag"));
    }

    @Override
    public void onDraw(View view, SpriteBatch batch, ShaderProgram shaderProgram, Texture texture) {
        if (scissorChanged) {
            float lowerX = scissor.getX(view);
            float lowerY = scissor.getY(view) - scissor.getHeight(view);
            float upperX = lowerX + scissor.getWidth(view);
            float upperY = lowerY + scissor.getHeight(view);

            shaderProgram.setUniformf(BOUNDS, lowerX, lowerY, upperX, upperY);

            scissorChanged = false;
        }
    }

    public void setScissor(Rectangle scissor) {
        assert scissor != null : "Scissor rectangle must not be null!";
        this.scissor = scissor;
        scissorChanged = true;
    }
}
