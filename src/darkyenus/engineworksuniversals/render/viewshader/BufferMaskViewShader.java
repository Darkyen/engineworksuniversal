package darkyenus.engineworksuniversals.render.viewshader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import darkyenus.engineworksuniversals.render.View;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/6/13
 * Time: 10:06 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class BufferMaskViewShader implements ViewShader {

    private static final String RESOLUTION = "resolution";
    private static final String MASK_TEXTURE = "maskTexture";
    private static final String CREATION_MODE = "creationMode";

    private FrameBuffer alphaBuffer;
    private boolean bufferChanged = false;
    private boolean creationMode = false;
    private boolean creationModeChanged = false;

    @Override
    public void size(int width, int height) {
        if (alphaBuffer != null) {
            alphaBuffer.dispose();
        }
        alphaBuffer = new FrameBuffer(Pixmap.Format.RGBA4444, width, height, false);
        bufferChanged = true;
    }

    public void beginMasking() {
        alphaBuffer.begin();
        creationMode = true;
        creationModeChanged = true;
    }

    public void endMasking() {
        alphaBuffer.end();
        creationMode = false;
        creationModeChanged = true;
    }

    @Override
    public ShaderProgram createShaderProgram() {
        bufferChanged = true;
        return View.createShader(Gdx.files.internal("darkyenus/engineworksuniversals/render/viewshader/shaderfiles/default.vert"), Gdx.files.internal("darkyenus/engineworksuniversals/render/viewshader/shaderfiles/buffermask.frag"));
    }

    @Override
    public void onDraw(View view, SpriteBatch batch, ShaderProgram shaderProgram, Texture texture) {
        if (bufferChanged) {
            shaderProgram.setUniformf(RESOLUTION, alphaBuffer.getWidth(), alphaBuffer.getHeight());
            alphaBuffer.getColorBufferTexture().bind(1);
            shaderProgram.setUniformi(MASK_TEXTURE, 1);
            Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0);
            bufferChanged = false;
        }
        if (creationModeChanged) {
            shaderProgram.setUniformi(CREATION_MODE, creationMode ? 1 : 0);
            creationModeChanged = false;
        }
    }
}
