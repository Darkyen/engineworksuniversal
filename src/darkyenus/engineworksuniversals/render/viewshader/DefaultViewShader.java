package darkyenus.engineworksuniversals.render.viewshader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import darkyenus.engineworksuniversals.render.View;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/5/13
 * Time: 4:57 PM
 */
public class DefaultViewShader implements ViewShader {

    @Override
    public void size(int width, int height) {
    }

    @Override
    public ShaderProgram createShaderProgram() {
        return View.createShader(Gdx.files.internal("darkyenus/engineworksuniversals/render/viewshader/shaderfiles/default.vert"), Gdx.files.internal("darkyenus/engineworksuniversals/render/viewshader/shaderfiles/default.frag"));
    }

    @Override
    public void onDraw(View view, SpriteBatch batch, ShaderProgram shaderProgram, Texture texture) {
    }
}
