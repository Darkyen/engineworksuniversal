package darkyenus.engineworksuniversals.render.viewshader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import darkyenus.engineworksuniversals.render.View;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/5/13
 * Time: 4:57 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class DeltaMaskViewShader implements ViewShader {

    public static final String MASK_OFFSET = "maskOffset";

    private Texture lastTexture = null;
    private float offsetX = 0f;
    private float offsetY = 0f;
    private boolean offsetChanged = false;


    @Override
    public void size(int width, int height) {
    }

    @Override
    public ShaderProgram createShaderProgram() {
        return View.createShader(Gdx.files.internal("darkyenus/engineworksuniversals/render/viewshader/shaderfiles/default.vert"), Gdx.files.internal("darkyenus/engineworksuniversals/render/viewshader/shaderfiles/deltamask.frag"));
    }

    @Override
    public void onDraw(View view, SpriteBatch batch, ShaderProgram shaderProgram, Texture texture) {
        if (offsetChanged || ((lastTexture == null || !lastTexture.equals(texture)) && !(offsetX == 0 && offsetY == 0))) {
            //Offset or Texture changed, update offsets
            batch.flush();
            lastTexture = texture;
            shaderProgram.setUniformf(MASK_OFFSET, offsetX / lastTexture.getWidth(), offsetY / lastTexture.getWidth());
            offsetChanged = false;
        }
    }

    /**
     * Gives shader info to start texturing with mask.
     * Mask works as follows:
     * Shader checks texture + x y, if that is black and opaque then it draws normally.
     * Otherwise it doesn't draw anything and discards.
     * This means that mask must be on the same texture as drawed image.
     *
     * @param offsetX of texture
     * @param offsetY of texture
     */
    public void startUseMask(float offsetX, float offsetY) {
        if (this.offsetX != offsetX || this.offsetY != offsetY) {
            this.offsetX = offsetX;
            this.offsetY = offsetY;
            offsetChanged = true;
        }
    }

    public void stopUseMask() {
        startUseMask(0, 0);
    }
}
