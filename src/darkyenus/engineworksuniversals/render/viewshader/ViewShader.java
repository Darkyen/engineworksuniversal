package darkyenus.engineworksuniversals.render.viewshader;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import darkyenus.engineworksuniversals.render.View;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/5/13
 * Time: 4:57 PM
 */
public interface ViewShader {

    /**
     * Called when shader loaded and when screen resized.
     *
     * @param width  of screen
     * @param height of screen
     */
    public void size(int width, int height);

    public ShaderProgram createShaderProgram();

    public void onDraw(View view, SpriteBatch batch, ShaderProgram shaderProgram, Texture texture);
}
