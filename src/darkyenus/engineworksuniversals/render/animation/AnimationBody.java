package darkyenus.engineworksuniversals.render.animation;

import darkyenus.engineworksuniversals.render.Renderable;
import darkyenus.engineworksuniversals.render.View;
import darkyenus.engineworksuniversals.render.animation.definitions.AnimationDefinition;
import darkyenus.utils.collections.LinkedCartCollection;
import darkyenus.utils.collections.Pair;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/23/13
 * Time: 4:33 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class AnimationBody implements Renderable{

    private BodyPart[] bodyParts;
    private List<BodyPart> temporaryBodyParts;
    private LinkedCartCollection<Pair<Animation, Animation.AnimationListener>> activeAnimations;

    public AnimationBody(BodyPart[] bodyParts) {
        this.bodyParts = bodyParts;
        temporaryBodyParts = new ArrayList<BodyPart>(0);//It is zero, because most AnimationBodies won't have any temporary parts
        activeAnimations = new LinkedCartCollection<Pair<Animation, Animation.AnimationListener>>();
    }

    public void setBodyPart(int id, BodyPart bodyPart) {
        bodyParts[id] = bodyPart;
    }

    public int getBodyPartID(String bodyPartName) {
        for (int i = 0; i < bodyParts.length; i++) {
            if (bodyParts[i] != null) {
                if (bodyPartName.equals(bodyParts[i].getName())) {
                    return i;
                }
            }
        }
        return -1;
    }

    public BodyPart getBodyPart(int id) {
        return bodyParts[id];
    }

    public void attachTemporaryBodyPart(BodyPart bodyPart) {
        temporaryBodyParts.add(bodyPart);
    }

    public void clearTemporaryBodyParts() {
        temporaryBodyParts.clear();
    }

    public Animation attachAnimation(AnimationDefinition animationDefinition) {
        Animation animation = animationDefinition.createAnimation();
        activeAnimations.add(new Pair<Animation, Animation.AnimationListener>(animation, null));
        return animation;
    }

    public Animation attachAnimation(AnimationDefinition animationDefinition, Animation.AnimationListener listener) {
        Animation animation = animationDefinition.createAnimation();
        activeAnimations.add(new Pair<Animation, Animation.AnimationListener>(animation, listener));
        listener.onAnimationStart(this, animation);
        return animation;
    }

    public Animation attachAnimation(Animation animation) {
        activeAnimations.add(new Pair<Animation, Animation.AnimationListener>(animation, null));
        return animation;
    }

    public Animation attachAnimation(Animation animation, Animation.AnimationListener listener) {
        activeAnimations.add(new Pair<Animation, Animation.AnimationListener>(animation, listener));
        listener.onAnimationStart(this, animation);
        return animation;
    }

    public void clearAnimations() {
        activeAnimations.clear();
    }

    @Override
    public void update(int deltaMs) {
        for (LinkedCartCollection.Cart<Pair<Animation, Animation.AnimationListener>> cart = activeAnimations.getFirst(); cart != null; cart = cart.getNext()) {
            Animation animation = cart.getEntry().getFirst();
            if (!animation.advance(deltaMs)) {
                cart.remove();
                cart.getEntry().getSecond().onAnimationEnd(this, animation);
            }
        }

        for (int i = 0; i < bodyParts.length; i++) {
            if (bodyParts[i] != null && bodyParts[i].isVisible()) {
                bodyParts[i].clearAnimationDeltas();
                for (LinkedCartCollection.Cart<Pair<Animation, Animation.AnimationListener>> cart = activeAnimations.getFirst(); cart != null; cart = cart.getNext()) {
                    cart.getEntry().getFirst().update(i, bodyParts[i]);
                }
            }
        }
    }

    public BodyPart[] getBodyParts() {
        return bodyParts;
    }

    public void setBodyParts(BodyPart[] bodyParts) {
        this.bodyParts = bodyParts;
    }

    @Override
    public void render(float x, float y, float scale, float rotation, View view) {
        view.pushState();//scale * point_translation * rotation * object_translation

        view.scale(1 / scale);
        //view.translate(x+(-x+view.getX()), y+(-y+view.getY()));
        view.translate(x,y);
        view.rotate(-rotation);
        //view.translate(-x+view.getX(),-y+view.getY());

        for (BodyPart bodyPart : bodyParts) {
            if (bodyPart != null && bodyPart.isVisible()) {
                bodyPart.render(view);
            }
        }
        for (Iterator<BodyPart> iterator = temporaryBodyParts.iterator(); iterator.hasNext(); ) {
            BodyPart bodyPart = iterator.next();
            if (bodyPart.isVisible()) {
                bodyPart.render(view);
            } else {
                iterator.remove();
            }
        }

        view.popState();
    }
}
