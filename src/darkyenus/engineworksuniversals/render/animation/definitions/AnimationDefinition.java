package darkyenus.engineworksuniversals.render.animation.definitions;

import darkyenus.engineworksuniversals.render.animation.Animation;
import darkyenus.engineworksuniversals.render.animation.AnimationFrame;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/26/13
 * Time: 4:01 PM
 */
public class AnimationDefinition {

    private boolean repeat;
    private boolean pingPong;
    private AnimationFrame[] states = new AnimationFrame[0];

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public boolean isPingPong() {
        return pingPong;
    }

    public void setPingPong(boolean pingPong) {
        this.pingPong = pingPong;
    }

    public AnimationFrame[] getStates() {
        return states;
    }

    public void setStates(AnimationFrame[] states) {
        this.states = states;
    }

    public Animation createAnimation() {
        return new Animation(this);
    }
}
