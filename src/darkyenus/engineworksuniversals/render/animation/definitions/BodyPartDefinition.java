package darkyenus.engineworksuniversals.render.animation.definitions;

import darkyenus.engineworksuniversals.render.animation.BodyPart;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/26/13
 * Time: 6:11 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class BodyPartDefinition {
    private float defaultX, defaultY, defaultRotation, defaultScale = 1;
    private String name;
    private String renderData;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getRenderData() {
        return renderData;
    }

    public void setRenderData(String renderData) {
        this.renderData = renderData;
    }

    public float getDefaultX() {
        return defaultX;
    }

    public BodyPartDefinition setDefaultX(float x) {
        this.defaultX = x;
        return this;
    }

    public float getDefaultY() {
        return defaultY;
    }

    public BodyPartDefinition setDefaultY(float y) {
        this.defaultY = y;
        return this;
    }

    public float getDefaultRotation() {
        return defaultRotation;
    }

    public BodyPartDefinition setDefaultRotation(float rotation) {
        this.defaultRotation = rotation;
        return this;
    }

    public float getDefaultScale() {
        return defaultScale;
    }

    public BodyPartDefinition setDefaultScale(float scale) {
        this.defaultScale = scale;
        return this;
    }

    public BodyPart create() {
        return new BodyPart(this);
    }
}
