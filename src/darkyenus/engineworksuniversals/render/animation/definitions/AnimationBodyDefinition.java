package darkyenus.engineworksuniversals.render.animation.definitions;

import darkyenus.engineworksuniversals.render.animation.AnimationBody;
import darkyenus.engineworksuniversals.render.animation.BodyPart;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/26/13
 * Time: 6:11 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class AnimationBodyDefinition {
    private BodyPartDefinition[] bodyParts;

    public BodyPartDefinition[] getBodyParts() {
        return bodyParts;
    }

    public void setBodyParts(BodyPartDefinition[] bodyParts) {
        this.bodyParts = bodyParts;
    }

    public AnimationBody create() {
        BodyPart[] bodyParts = new BodyPart[this.bodyParts.length];
        for (int i = 0; i < bodyParts.length; i++) {
            bodyParts[i] = this.bodyParts[i].create();
        }
        return new AnimationBody(bodyParts);
    }
}
