package darkyenus.engineworksuniversals.render.animation;

import darkyenus.engineworksuniversals.render.animation.definitions.AnimationDefinition;
import darkyenus.engineworksuniversals.tween.Tween;
import darkyenus.utils.InterpolationUtils;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/23/13
 * Time: 4:42 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class Animation {

    private AnimationDefinition definition;
    private boolean cancelled = false;
    private int timeRemainingAtState = 0;
    private boolean forward = true;

    private int state = -1;
    private int nextState = 0;

    public Animation(AnimationDefinition definition) {
        this.definition = definition;
    }

    /**
     * Advances this animation by given amount of ms.
     *
     * @param deltaMs by how much update Animation timer.
     * @return whether animation can continue; true = do not remove; false = animation done, remove me
     */
    public boolean advance(int deltaMs) {
        if (cancelled) {
            return false;
        }

        timeRemainingAtState -= deltaMs;
        if (timeRemainingAtState <= 0) {
            state = getStateAfter(state, true);
            nextState = getStateAfter(state, false);

            if (state == -1 || nextState == -1) {
                return false;
            }

            timeRemainingAtState = definition.getStates()[state].getDuration();
        }
        return true;
    }

    private int getStateAfter(int i, boolean doChanges) {
        int result;
        if (forward) {
            result = i + 1;
            if (result == definition.getStates().length) {
                if (definition.isRepeat()) {
                    if (definition.isPingPong()) {
                        if (doChanges)
                            forward = false;
                        result = i;
                    } else {
                        result = 0;
                    }
                } else {
                    return -1;
                }
            }
        } else {
            result = i - 1;
            if (result < 0) {
                if (doChanges)
                    forward = true;
                result = i;
            }
        }
        return result;
    }

    public void cancel() {
        cancelled = true;
    }

    /**
     * Apply animation offsets to given body part.
     *
     * @param id       of body part
     * @param bodyPart body part to animate
     */
    public void update(int id, AnimationableBodyPart bodyPart) {
        BodyPartDeltaState deltaState = definition.getStates()[state].getDeltaState(id);
        BodyPartDeltaState nextDeltaState = definition.getStates()[nextState].getDeltaState(id);
        Tween tween = definition.getStates()[state].getTweenToContinue();
        if (deltaState != null && nextDeltaState != null && tween != null) {
            float progressRaw = (float) timeRemainingAtState / (float) definition.getStates()[state].getDuration();
            float prog = 1f - tween.get(progressRaw);

            bodyPart.addAnimationX(lerp(deltaState.getDeltaX(), nextDeltaState.getDeltaX(), prog));
            bodyPart.addAnimationY(lerp(deltaState.getDeltaY(), nextDeltaState.getDeltaY(), prog));
            bodyPart.addAnimationRotation(lerp(deltaState.getDeltaRotation(), nextDeltaState.getDeltaRotation(), prog));
            bodyPart.addAnimationScale(lerp(deltaState.getDeltaScale(), nextDeltaState.getDeltaScale(), prog));
        }
    }

    private float lerp(float from, float to, float progress) {
        return InterpolationUtils.interpolateLinear(from, to, progress);
    }

    private float rerp(float from, float to, float progress) {
        return InterpolationUtils.interpolateRadian(from, to, progress);
    }


    public interface AnimationListener {
        public void onAnimationStart(AnimationBody body, Animation animation);

        public void onAnimationEnd(AnimationBody body, Animation animation);
    }
}
