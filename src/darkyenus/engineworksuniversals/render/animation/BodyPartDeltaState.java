package darkyenus.engineworksuniversals.render.animation;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/23/13
 * Time: 6:00 PM
 */
public class BodyPartDeltaState {
    private float deltaX, deltaY, deltaRotation, deltaScale;

    public float getDeltaX() {
        return deltaX;
    }

    public void setDeltaX(float deltaX) {
        this.deltaX = deltaX;
    }

    public float getDeltaY() {
        return deltaY;
    }

    public void setDeltaY(float deltaY) {
        this.deltaY = deltaY;
    }

    public float getDeltaRotation() {
        return deltaRotation;
    }

    public void setDeltaRotation(float deltaRotation) {
        this.deltaRotation = deltaRotation;
    }

    public float getDeltaScale() {
        return deltaScale;
    }

    public void setDeltaScale(float deltaScale) {
        this.deltaScale = deltaScale;
    }
}
