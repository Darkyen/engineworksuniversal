package darkyenus.engineworksuniversals.render.animation;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/23/13
 * Time: 5:39 PM
 */
public interface AnimationableBodyPart {

    public float getAnimationX();

    public float getAnimationY();

    public float getAnimationRotation();

    public float getAnimationScale();

    public BodyPart addAnimationX(float animationX);

    public BodyPart addAnimationY(float animationY);

    public BodyPart addAnimationRotation(float animationRotation);

    public BodyPart addAnimationScale(float animationScale);
}
