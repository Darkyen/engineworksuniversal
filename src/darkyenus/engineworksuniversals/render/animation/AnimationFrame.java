package darkyenus.engineworksuniversals.render.animation;

import darkyenus.engineworksuniversals.tween.Tween;

import java.util.HashMap;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/23/13
 * Time: 4:42 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class AnimationFrame {
    private HashMap<Integer, BodyPartDeltaState> deltaStates = new HashMap<Integer, BodyPartDeltaState>();
    private int duration = 0;
    private Tween tweenToContinue;

    public AnimationFrame(int duration) {
        this(duration, Tween.LINEAR);
    }

    public AnimationFrame(int duration, Tween tweenToContinue) {
        this.duration = duration;
        this.tweenToContinue = tweenToContinue;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Tween getTweenToContinue() {
        return tweenToContinue;
    }

    public void setTweenToContinue(Tween tweenToContinue) {
        this.tweenToContinue = tweenToContinue;
    }

    public void putDeltaState(int part, BodyPartDeltaState deltaState) {
        deltaStates.put(part, deltaState);
    }

    public BodyPartDeltaState getDeltaState(int bodyPart) {
        return deltaStates.get(bodyPart);
    }
}
