package darkyenus.engineworksuniversals.render.animation;

import darkyenus.engineworksuniversals.render.Renderable;
import darkyenus.engineworksuniversals.render.ResourceManager;
import darkyenus.engineworksuniversals.render.View;
import darkyenus.engineworksuniversals.render.animation.definitions.BodyPartDefinition;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/23/13
 * Time: 4:35 PM
 */
public class BodyPart implements AnimationableBodyPart {

    private boolean visible = true;
    private float animationX, animationY, animationRotation, animationScale;
    private BodyPartDefinition definition;

    public BodyPart(BodyPartDefinition definition) {
        this.definition = definition;
    }

    public String getName() {
        return definition.getName();
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void clearAnimationDeltas() {
        animationX = definition.getDefaultX();
        animationY = definition.getDefaultY();
        animationScale = definition.getDefaultScale();
        animationRotation = definition.getDefaultRotation();
    }

    public float getAnimationX() {
        return animationX;
    }

    public float getAnimationY() {
        return animationY;
    }

    public float getAnimationRotation() {
        return animationRotation;
    }

    public float getAnimationScale() {
        return animationScale;
    }

    public BodyPart addAnimationX(float animationX) {
        this.animationX += animationX;
        return this;
    }

    public BodyPart addAnimationY(float animationY) {
        this.animationY += animationY;
        return this;
    }

    public BodyPart addAnimationRotation(float animationRotation) {
        this.animationRotation += animationRotation;
        return this;
    }

    public BodyPart addAnimationScale(float animationScale) {
        this.animationScale += animationScale;
        return this;
    }

    public void render(View view) {
        ResourceManager.get(Renderable.class, definition.getRenderData()).render(getAnimationX(), getAnimationY(), getAnimationScale(), getAnimationRotation(), view);
    }
}
