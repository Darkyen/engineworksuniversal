package darkyenus.engineworksuniversals.render.animation;

import darkyenus.engineworksuniversals.render.animation.definitions.AnimationDefinition;

import java.util.HashMap;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/25/13
 * Time: 12:13 PM
 */
public class AnimationLibrary extends HashMap<String, AnimationDefinition> {
    public Animation create(String key) {
        AnimationDefinition definition = get(key);
        assert definition != null : "AnimationDefinition not found!";
        return definition.createAnimation();
    }
}
