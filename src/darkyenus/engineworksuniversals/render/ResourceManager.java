package darkyenus.engineworksuniversals.render;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.BitmapFontLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import darkyenus.engineworksuniversals.EngineworksError;
import darkyenus.engineworksuniversals.logging.AbstractLogger;
import darkyenus.engineworksuniversals.logging.SimpleLogger;
import darkyenus.engineworksuniversals.render.animation.AnimationLibrary;
import darkyenus.engineworksuniversals.render.animation.definitions.AnimationBodyDefinition;
import darkyenus.engineworksuniversals.render.animation.definitions.AnimationDefinition;
import darkyenus.engineworksuniversals.render.renderables.BasicImage;
import darkyenus.engineworksuniversals.render.renderables.CorneredImage;
import darkyenus.engineworksuniversals.render.renderables.Image;
import darkyenus.engineworksuniversals.render.renderables.SolidImage;
import darkyenus.utils.StringUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * Extended and static AssetManager.
 * <p/>
 * ResourceIdentification syntax:
 * Things in "()" must be in "()", things in "[]" must be written as data without "[]".
 * Things in "<>" are optional.
 * <p/>
 * Resource part:
 * [Type] \([Data part]\) <[Alias]>
 * <p/>
 * For [Data part] specification, see loader of given [Type].
 * <p/>
 * Aliasing:
 * If type is specified as ALIAS_TYPE, [Data part] is replaced with [Data part]
 * of already loaded resource, which had appended an [Alias] tag.
 * NOTES:
 * - Aliases can have aliases too, however, you probably don't want to do that.
 * - There is an assertion that you won't load multiple ResourceIdentifiers with same [Alias] tag.
 * This would result in using the latest ResourceIdentificator in case of loading something aliased.
 * - Use r and rA methods for building resource strings at runtime, it is less prone to syntax mistakes
 * <p/>
 * <p/>
 * <p/>
 * Private property.
 * User: Darkyen
 * Date: 5/10/13
 * Time: 12:14 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class ResourceManager {

    public static String ALIAS_TYPE = "Alias";
    private static AssetManager manager = new AssetManager();
    private static HashMap<String, Resource> aliases = new HashMap<String, Resource>();
    private static HashMap<String, Resource> resources = new HashMap<String, Resource>();
    private static HashMap<String, ResourceLoader> resourceLoaders = new HashMap<String, ResourceLoader>();
    private static AbstractLogger logger = new SimpleLogger(AbstractLogger.Importance.CRITICAL, System.out);

    static {
        addLoader(new ImageLoader());
        addLoader(new ImageSheetLoader());
        addLoader(new ImageSheetImageLoader());
        addLoader(new FontLoader());
        addLoader(new CorneredImageLoader());
        addLoader(new SolidImageLoader());
        addLoader(new AnimationBodyLoader());
        addLoader(new AnimationLibraryLoader());
        addLoader(new AnimationLoader());
    }

    public static void addLoader(ResourceLoader loader) {
        resourceLoaders.put(loader.getResourceType(), loader);
    }

    public static void load(ResourceIdentifier... identifiers) {
        for (ResourceIdentifier identificator : identifiers) {
            try {
                Resource resource = new Resource(identificator.getDataKey());
                resources.put(identificator.getDataKey(), resource.load());
            } catch (Throwable ex) {
                throw new EngineworksError("Exception while loading identificator with key \"" + identificator.getDataKey() + "\"", ex);
            }
        }
    }

    public static void load(String... identifiers) {
        for (String identificator : identifiers) {
            try {
                Resource resource = new Resource(identificator);
                resources.put(identificator, resource.load());
            } catch (Exception ex) {
                throw new EngineworksError("Exception while loading key \"" + identificator + "\"", ex);
            }
        }
    }

    public static <T> T get(Class<T> type, ResourceIdentifier identificator) {
        return get(type, identificator.getDataKey());
    }

    @SuppressWarnings("unchecked")
    public static <T> T get(Class<T> type, String identificatorKey) {
        Resource resource = resources.get(identificatorKey);
        if (resource == null) {
            load(identificatorKey);
            finishLoading();
            resource = resources.get(identificatorKey);
        }
        Object result = resource.getData();
        assert type.isInstance(result);
        return (T) result;
    }

    public static void updateLoading(int millis) {
        manager.update(millis);
    }

    public static void updateLoading() {
        manager.update();
    }

    public static void finishLoading() {
        manager.finishLoading();
    }

    public static float getLoadingProgress() {
        return manager.getProgress();
    }

    public static boolean doneLoading() {
        return manager.getQueuedAssets() == 0;
    }

    public static AssetManager getAssetManager() {
        return manager;
    }

    public static void disposeAll() {
        manager.clear();
        aliases.clear();
        resources.clear();
    }

    public static void setLogger(AbstractLogger logger) {
        assert logger != null;
        ResourceManager.logger = logger;
    }

    public static int countResources() {
        return resources.size();
    }

    public static int countAliases() {
        return aliases.size();
    }

    private static class Resource {
        private String typeName;
        private String[] resourceKeyParams;
        private String alias = null;
        private Object data;

        private Resource(String resourceIdentificatorDataKey) {
            String[] data = parse(resourceIdentificatorDataKey);
            typeName = data[0];
            resourceKeyParams = parse(data[1]);
            if (data.length >= 3) {
                alias = data[2];
            }
        }

        public Object getData() {
            if (data == null) {
                ResourceLoader loader = resourceLoaders.get(typeName);
                assert loader != null;
                try {
                    data = loader.loadResource(resourceKeyParams);
                } catch (Throwable ex) {
                    logger.log(AbstractLogger.Importance.CRITICAL, "Could not load resource.", ex);
                }
            }
            return data;
        }

        public Resource load() {
            Resource result;
            if (typeName.equals(ALIAS_TYPE)) {
                result = aliases.get(resourceKeyParams[0]);
            } else {
                ResourceLoader loader = resourceLoaders.get(typeName);
                assert loader != null;
                loader.addToLoadingQueue(resourceKeyParams);
                result = this;
            }

            if (alias != null) {
                assert !aliases.containsKey(alias);
                aliases.put(alias, result);
            }
            return result;
        }
    }

    public static interface ResourceIdentifier {
        public String getDataKey();
    }

    public static interface ResourceLoader<R> {
        public String getResourceType();

        public void addToLoadingQueue(String[] params);

        public R loadResource(String[] params) throws Throwable;
    }

    //Utility methods

    private static String[] parse(String identificator) {
        return StringUtil.splitContextAware(identificator, " ", '(', ')', false);
    }

    private static int toInt(String string) {
        return Integer.parseInt(string);
    }

    /**
     * Convenience method for creating Identifiers.
     * Full name would be resourceAlias.
     * <p/>
     * Use following imports if you want:
     * import static darkyenus.engineworks.ResourceManager.r;
     * import static darkyenus.engineworks.ResourceManager.rA;
     *
     * @param type  type of identificator
     * @param alias alias of identificator
     * @param data  of identificator
     * @return Full Identificator string
     */
    public static String rA(String type, String alias, Object... data) {
        StringBuilder result = new StringBuilder(type);
        result.append(' ').append('(');
        result.append('(').append(data[0].toString()).append(')');
        for (int i = 1; i < data.length; i++) {
            result.append(' ').append('(').append(data[i].toString()).append(')');
        }
        result.append(')').append(' ');
        result.append(alias);
        return result.toString();
    }

    /**
     * Convenience method for creating Identifiers.
     * Full name would be resource.
     * <p/>
     * Use following imports if you want:
     * import static darkyenus.engineworks.ResourceManager.r;
     * import static darkyenus.engineworks.ResourceManager.rA;
     *
     * @param type type of identificator
     * @param data of identificator
     * @return Full Identificator string
     */
    public static String r(String type, Object... data) {
        StringBuilder result = new StringBuilder(type);
        result.append(' ').append('(');
        result.append('(').append(data[0].toString()).append(')');
        for (int i = 1; i < data.length; i++) {
            result.append(' ').append('(').append(data[i].toString()).append(')');
        }
        result.append(')');
        return result.toString();
    }

    /**
     * [Path to image] < Optional parameters - see below>
     * You can also specify some other parameters:
     * filter:[Texture.TextureFilter]
     * Specifies min and mag filter in one parameter.
     * Possible values can be found in com.badlogic.gdx.graphics.Texture.TextureFilter enum.
     * Case sensitive.
     * minfilter:[Texture.TextureFilter]
     * magfilter:[Texture.TextureFilter]
     * Specify filters separately, see 'filter'.
     * genmipmap
     * Add this parameter to generate mipmaps.
     * format:[Pixmap.Format]
     * Specify format of image.
     * Possible values can be found in com.badlogic.gdx.graphics.Pixmap.Format enum.
     * mask:[Image]
     * Specify masking image for use in shaders.
     * [Image] has same format as ResourceType.IMAGE.
     * coordinates:[x],[y],[width],[height]
     * Specify Image's coordinates on texture.
     * Has results comparable to loading IMAGE_SHEET_IMAGE but more dynamic.
     * Example of optional parameter usage:
     * path/to/image.png filter:Linear genmipmap coordinates:0,0,10,10 mask:(path/to/image.png coordinates:10,0,10,10)
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static class ImageLoader implements ResourceLoader<Image> {

        public static String TYPE = "Image";

        @Override
        public String getResourceType() {
            return TYPE;
        }

        @Override
        public void addToLoadingQueue(String[] params) {
            if (!manager.isLoaded(params[0], Texture.class)) {
                TextureLoader.TextureParameter parameters = new TextureLoader.TextureParameter();

                for (int i = 1; i < params.length; i++) {
                    String line = params[i];
                    if (line.startsWith("filter:")) {
                        parameters.minFilter = Texture.TextureFilter.valueOf(line.substring(7));
                        parameters.magFilter = Texture.TextureFilter.valueOf(line.substring(7));
                    } else if (line.startsWith("minfilter:")) {
                        parameters.minFilter = Texture.TextureFilter.valueOf(line.substring(7));
                    } else if (line.startsWith("magfilter:")) {
                        parameters.magFilter = Texture.TextureFilter.valueOf(line.substring(7));
                    } else if (line.startsWith("genmipmap")) {
                        parameters.genMipMaps = true;
                    } else if (line.startsWith("format:")) {
                        parameters.format = Pixmap.Format.valueOf(line.substring(7));
                    } else if (line.startsWith("mask:")) {
                        load(line.substring(5));
                    }
                }

                manager.load(params[0], Texture.class, parameters);
            }
        }

        @Override
        public Image loadResource(String[] params) {
            Image result = new BasicImage(manager.get(params[0], Texture.class));
            for (int i = 1; i < params.length; i++) {
                String line = params[i];
                if (line.startsWith("mask:")) {
                    result.setMask(get(Image.class, line.substring(5)));
                } else if (line.startsWith("coordinates:")) {
                    String[] coordinates = line.substring(12).split(",");
                    result = result.duplicate();//Important, see ImageSheetImage
                    result.setX(toInt(coordinates[0]));
                    result.setY(toInt(coordinates[1]));
                    result.setWidth(toInt(coordinates[2]));
                    result.setHeight(toInt(coordinates[3]));
                }
            }
            return result;
        }
    }

    /**
     * [float:r] [float:g] [float:b] <[float:a]>
     */
    public static class SolidImageLoader implements ResourceLoader<Image> {

        public static String TYPE = "SolidImage";

        @Override
        public String getResourceType() {
            return TYPE;
        }

        @Override
        public void addToLoadingQueue(String[] params) {
        }

        @Override
        public Image loadResource(String[] params) {
            return params.length >= 6 ? new SolidImage(toInt(params[0]), toInt(params[1]), new Color(Float.parseFloat(params[2]), Float.parseFloat(params[3]), Float.parseFloat(params[4]), Float.parseFloat(params[5]))) : new SolidImage(toInt(params[0]), toInt(params[1]), new Color(Float.parseFloat(params[2]), Float.parseFloat(params[3]), Float.parseFloat(params[4]), 1f));
        }
    }

    /**
     * (Image) [Cell Width] [Cell Height]
     */
    public static class ImageSheetLoader implements ResourceLoader<Image[][]> {

        public static String TYPE = "ImageSheet";

        @Override
        public String getResourceType() {
            return TYPE;
        }

        @Override
        public void addToLoadingQueue(String[] params) {
            load(params[0]);
        }

        @Override
        public Image[][] loadResource(String[] params) {
            Image image = get(Image.class, params[0]);
            return image.split(toInt(params[1]), toInt(params[2]));
        }
    }

    /**
     * (Image Sheet) [X] [Y]
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static class ImageSheetImageLoader implements ResourceLoader<Image> {

        public static String TYPE = "ImageSheetImage";

        @Override
        public String getResourceType() {
            return TYPE;
        }

        @Override
        public void addToLoadingQueue(String[] params) {
            load(params[0]);
        }

        @Override
        public Image loadResource(String[] params) {
            Image[][] regions = get(Image[][].class, params[0]);
            Image result = regions[toInt(params[1])][toInt(params[2])];
            for (int i = 3; i < params.length; i++) {
                String line = params[i];
                if (line.startsWith("mask:")) {
                    result.setMask(get(Image.class, line.substring(5)));
                } else if (line.startsWith("coordinates:")) {
                    String[] coordinates = line.substring(12).split(",");
                    int x = toInt(coordinates[0]);
                    int y = toInt(coordinates[1]);
                    int width = toInt(coordinates[2]);
                    int height = toInt(coordinates[3]);
                    result = result.duplicate();//Very important, see below
                    /*
                    * (ImageSheetImage ((ImageSheet ((Image (Images/bodyparts.png)) 64 64) BodyPartSheet) 1 0 coordinates:0,0,32,64) Leg)
                    * (ImageSheetImage ((Alias BodyPartSheet) 1 0 coordinates:32,0,32,64) Arm)
                    * This would set that image's coordinates to Arm's coordinates, but luckily, there's duplication so it doesn't happen.
                    * */
                    result.setX(result.getX() + x);
                    result.setY(result.getY() + y);
                    result.setWidth(width);
                    result.setHeight(height);
                }
            }
            return result;
        }
    }

    /**
     * [Path to font specification]
     */
    public static class FontLoader implements ResourceLoader<BitmapFont> {

        public static String TYPE = "Font";

        @Override
        public String getResourceType() {
            return TYPE;
        }

        @Override
        public void addToLoadingQueue(String[] params) {
            if (!manager.isLoaded(params[0], BitmapFont.class)) {
                BitmapFontLoader.BitmapFontParameter parameter = new BitmapFontLoader.BitmapFontParameter();
                parameter.flip = true;
                manager.load(params[0], BitmapFont.class, parameter);
            }
        }

        @Override
        public BitmapFont loadResource(String[] params) {
            return manager.get(params[0], BitmapFont.class);
        }
    }

    /**
     * (Image Sheet Image) [Left Width] [Middle Width] [Right Width] [Top Height] [Middle Height] [Bottom Height]
     */
    public static class CorneredImageLoader implements ResourceLoader<CorneredImage> {

        public static String TYPE = "CorneredImage";

        @Override
        public String getResourceType() {
            return TYPE;
        }

        @Override
        public void addToLoadingQueue(String[] params) {
            load(params[0]);
        }

        @Override
        public CorneredImage loadResource(String[] params) {
            Image image = get(Image.class, params[0]);
            return new CorneredImage(image, new int[]{toInt(params[1]), toInt(params[2]), toInt(params[3]), toInt(params[4])});
        }
    }

    /**
     * [File with serialized body definition]
     */
    public static class AnimationBodyLoader implements ResourceLoader<AnimationBodyDefinition> {

        public static String TYPE = "AnimationBody";

        @Override
        public String getResourceType() {
            return TYPE;
        }

        @Override
        public void addToLoadingQueue(String[] params) {
        }

        @Override
        public AnimationBodyDefinition loadResource(String[] params) throws IOException {
            Kryo kryo = new Kryo();
            FileInputStream stream = new FileInputStream(params[0]);
            AnimationBodyDefinition result = kryo.readObject(new Input(stream), AnimationBodyDefinition.class);
            stream.close();
            return result;
        }
    }

    /**
     * [File with serialized animation library]
     */
    public static class AnimationLibraryLoader implements ResourceLoader<AnimationLibrary> {

        public static String TYPE = "AnimationLibrary";

        @Override
        public String getResourceType() {
            return TYPE;
        }

        @Override
        public void addToLoadingQueue(String[] params) {
        }

        @Override
        public AnimationLibrary loadResource(String[] params) throws IOException {
            Kryo kryo = new Kryo();
            FileInputStream stream = new FileInputStream(params[0]);
            AnimationLibrary result = kryo.readObject(new Input(stream), AnimationLibrary.class);
            stream.close();
            return result;
        }
    }

    /**
     * (AnimationLibrary) [Animation]
     */
    public static class AnimationLoader implements ResourceLoader<AnimationDefinition> {

        public static String TYPE = "Animation";

        @Override
        public String getResourceType() {
            return TYPE;
        }

        @Override
        public void addToLoadingQueue(String[] params) {
            load(params[0]);
        }

        @Override
        public AnimationDefinition loadResource(String[] params) throws Throwable {
            AnimationLibrary library = get(AnimationLibrary.class, params[0]);
            return library.get(params[1]);
        }
    }
}
