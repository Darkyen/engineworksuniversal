package darkyenus.engineworksuniversals.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import darkyenus.engineworksuniversals.geometry.Dimension;
import darkyenus.engineworksuniversals.render.viewshader.DefaultViewShader;
import darkyenus.engineworksuniversals.render.viewshader.ViewShader;
import darkyenus.utils.collections.Pair;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Private property.
 * User: Darkyen
 * Date: 4/5/13
 * Time: 9:24 AM
 */
@SuppressWarnings("UnusedDeclaration")
public final class View implements Dimension {
    public static BitmapFont DEFAULT_FONT;
    public static Texture WHITE;
    private static boolean reloadShaders = false;

    private SpriteBatch spriteBatch;
    private boolean spriteBatchInUse = false;
    private ShapeRenderer shapeRenderer = new ShapeRenderer();
    private boolean shapeRendererInUse = false;
    private ShapeRenderer.ShapeType lastShapeType = null;
    private OrthographicCamera camera;
    private ArrayList<State> stateStack = new ArrayList<State>();
    private Color color = Color.WHITE.cpy();
    private BitmapFont font;
    private int width;
    private int height;
    private ViewShader activeShader;
    private ShaderProgram activeShaderProgram;
    private HashMap<Class<? extends ViewShader>, Pair<? extends ViewShader, ShaderProgram>> shaderLibrary = new HashMap<Class<? extends ViewShader>, Pair<? extends ViewShader, ShaderProgram>>();
    private float[] vertices = new float[20];

    public View(int width, int height) {
        this(width, height, new SpriteBatch(1000));
    }

    public View(int width, int height, SpriteBatch spriteBatch) {
        addShader(new DefaultViewShader());
        this.spriteBatch = spriteBatch;
        useShader(DefaultViewShader.class);

        camera = new OrthographicCamera(width, height);
        Gdx.graphics.getGL20().glClearColor(0.5f, 0.5f, 0.5f, 1);
        Gdx.graphics.getGL20().glClearStencil(0);
        DEFAULT_FONT = new BitmapFont(true);
        font = DEFAULT_FONT;

        Pixmap whitePixelPixmap = new Pixmap(1, 1, Pixmap.Format.RGB888);
        whitePixelPixmap.drawPixel(0, 0, Color.WHITE.toIntBits());
        WHITE = new Texture(whitePixelPixmap);
        clear(width, height);
    }

    /**
     * Used by overriding classes when static core of View has already been initialized.
     */
    protected View() {
    }

    public static ShaderProgram createShader(FileHandle vertexShader, FileHandle fragmentShader) {
        try {
            ShaderProgram shader = new ShaderProgram(vertexShader, fragmentShader);
            if (!shader.isCompiled()) throw new IllegalArgumentException("couldn't compile shader: " + shader.getLog());
            return shader;
        } catch (Throwable error) {
            throw new Error("Exception occurred while loading shader (Vertex: " + vertexShader.path() + " (Exists: " + vertexShader.exists() + "), Fragment: " + fragmentShader.path() + "(Exists: " + fragmentShader.exists() + "))", error);
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public ShapeRenderer getShapeRenderer(ShapeRenderer.ShapeType shapeType) {
        if (spriteBatchInUse) {
            spriteBatch.end();
            spriteBatchInUse = false;
        }
        if (!shapeRendererInUse) {
            shapeRenderer.begin(shapeType);
            lastShapeType = shapeType;
            shapeRendererInUse = true;
        } else if (!shapeType.equals(lastShapeType)) {
            //It is in use, but in bad use
            shapeRenderer.end();
            shapeRenderer.begin(shapeType);
            lastShapeType = shapeType;
        }
        return shapeRenderer;
    }

    public void fillRect(float x, float y, float width, float height) {
        float color = getColor().toFloatBits();
        int idx = 0;

        vertices[idx++] = x;
        vertices[idx++] = y;
        vertices[idx++] = color;
        vertices[idx++] = 0;
        vertices[idx++] = 0;

        vertices[idx++] = x + width;
        vertices[idx++] = y;
        vertices[idx++] = color;
        vertices[idx++] = 1;
        vertices[idx++] = 0;

        vertices[idx++] = x + width;
        vertices[idx++] = y + height;
        vertices[idx++] = color;
        vertices[idx++] = 1;
        vertices[idx++] = 1;

        vertices[idx++] = x;
        vertices[idx++] = y + height;
        vertices[idx++] = color;
        vertices[idx++] = 0;
        vertices[idx] = 1;

        getSpriteBatch().draw(WHITE, vertices, 0, vertices.length);
        activeShader.onDraw(this, getSpriteBatch(), activeShaderProgram, WHITE);
    }

    public void fillRect(float x, float y, float width, float height, float rotation) {
        if (rotation == 0) {
            fillRect(x, y, width, height);
            return;
        }
        float color = getColor().toFloatBits();
        int idx = 0;

        // bottom left and top right corner points relative to origin
        final float worldOriginX = x + width / 2;
        final float worldOriginY = y + height / 2;
        float fx = -width / 2;
        float fy = -height / 2;
        float fx2 = width / 2;
        float fy2 = height / 2;

        float x1;
        float y1;
        float x2;
        float y2;
        float x3;
        float y3;
        float x4;
        float y4;

        // rotate
        final float cos = MathUtils.cos(rotation);
        final float sin = MathUtils.sin(rotation);

        x1 = cos * fx - sin * fy;
        y1 = sin * fx + cos * fy;

        x2 = cos * fx - sin * fy2;
        y2 = sin * fx + cos * fy2;

        x3 = cos * fx2 - sin * fy2;
        y3 = sin * fx2 + cos * fy2;

        x4 = x1 + (x3 - x2);
        y4 = y3 - (y2 - y1);
        // /rotate

        x1 += worldOriginX;
        y1 += worldOriginY;
        x2 += worldOriginX;
        y2 += worldOriginY;
        x3 += worldOriginX;
        y3 += worldOriginY;
        x4 += worldOriginX;
        y4 += worldOriginY;

        vertices[idx++] = x1;
        vertices[idx++] = y1;
        vertices[idx++] = color;
        vertices[idx++] = 0;
        vertices[idx++] = 0;

        vertices[idx++] = x2;
        vertices[idx++] = y2;
        vertices[idx++] = color;
        vertices[idx++] = 1;
        vertices[idx++] = 0;

        vertices[idx++] = x3;
        vertices[idx++] = y3;
        vertices[idx++] = color;
        vertices[idx++] = 1;
        vertices[idx++] = 1;

        vertices[idx++] = x4;
        vertices[idx++] = y4;
        vertices[idx++] = color;
        vertices[idx++] = 0;
        vertices[idx] = 1;

        getSpriteBatch().draw(WHITE, vertices, 0, vertices.length);
        activeShader.onDraw(this, getSpriteBatch(), activeShaderProgram, WHITE);
    }

    public void drawString(String text, float x, float y) {
        drawString(text, x, y, false);
    }

    public void drawString(String text, float x, float y, boolean hasLines) {
        if (hasLines) {
            float lineY = y;
            String[] lines = text.split("\n");
            for (String line : lines) {
                font.draw(getSpriteBatch(), line, x, lineY);
                lineY += font.getLineHeight();
            }
        } else {
            font.draw(getSpriteBatch(), text, x, y);
        }
        activeShader.onDraw(this, getSpriteBatch(), activeShaderProgram, font.getRegion().getTexture());
    }

    public void drawStringCentered(String text, float x, float y) {
        drawString(text, x - font.getBounds(text).width / 2, y - font.getLineHeight() / 2);
    }

    public void drawStringCentered(String text, float x, float y, boolean hasLines) {
        drawString(text, x - font.getBounds(text).width / 2, y - font.getLineHeight() / 2, hasLines);
    }

    public void drawStringWrapped(String text, float x, float y, float wrapWidth) {
        font.drawWrapped(getSpriteBatch(), text, x, y, wrapWidth);
        activeShader.onDraw(this, getSpriteBatch(), activeShaderProgram, font.getRegion().getTexture());
    }

    public BitmapFont getFont() {
        return font;
    }

    public void setFont(BitmapFont font) {
        if (font != null) {
            this.font = font;
            font.setColor(color);
        } else {
            throw new IllegalArgumentException("Font can't be null.");
        }
    }

    private void updateColor() {
        shapeRenderer.setColor(color);
        spriteBatch.setColor(color);
        font.setColor(color);
    }

    public void multiplyColor(Color color) {
        color.mul(color);
        updateColor();
    }

    /**
     * Get color which is used by this view and its subsystems.
     * This color is a direct reference, do not modify!
     *
     * @return used color
     */
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color.set(color);
        updateColor();
    }

    public void setColor(float r, float g, float b) {
        setColor(r, g, b, 1f);
    }

    public void setColor(float r, float g, float b, float a) {
        this.color.set(r, g, b, a);
        updateColor();
    }

    public void addShader(ViewShader shader) {
        shaderLibrary.put(shader.getClass(), new Pair<ViewShader, ShaderProgram>(shader, shader.createShaderProgram()));
        shader.size(getWidth(), getHeight());
    }

    @SuppressWarnings("unchecked")
    public <T extends ViewShader> T useShader(Class<T> shader) {
        if (!shader.isInstance(this.activeShader)) {
            end();
            Pair<? extends ViewShader, ShaderProgram> pairToUse = shaderLibrary.get(shader);
            assert pairToUse != null : "Shader was not added yet!";

            this.activeShader = pairToUse.getFirst();
            this.activeShaderProgram = pairToUse.getSecond();
            this.spriteBatch.setShader(this.activeShaderProgram);
        }
        return (T) this.activeShader;
    }

    @SuppressWarnings("unchecked")
    public <T extends ViewShader> T getShader(Class<T> type) {
        Pair<? extends ViewShader, ShaderProgram> pairToUse = shaderLibrary.get(type);
        assert type.isInstance(pairToUse.getFirst());
        return (T) pairToUse.getFirst();
    }

    public ViewShader getActiveShader() {
        return activeShader;
    }

    public ShaderProgram getActiveShaderProgram() {
        return activeShaderProgram;
    }

    public void reloadShaders() {
        end();
        for (Pair<? extends ViewShader, ShaderProgram> pair : shaderLibrary.values()) {
            pair.getSecond().dispose();
            pair.setSecond(pair.getFirst().createShaderProgram());
        }

        Pair<? extends ViewShader, ShaderProgram> pairToUse = shaderLibrary.get(this.activeShader.getClass());
        this.activeShaderProgram = pairToUse.getSecond();
        this.spriteBatch.setShader(this.activeShaderProgram);
    }

    public SpriteBatch getSpriteBatch() {
        if (shapeRendererInUse) {
            shapeRenderer.end();
            shapeRendererInUse = false;
        }

        if (!spriteBatchInUse) {
            spriteBatch.begin();
            spriteBatchInUse = true;
        }

        return spriteBatch;
    }

    public void flush() {
        if (spriteBatchInUse) {
            spriteBatch.flush();
        }

        if (shapeRendererInUse) {
            shapeRenderer.flush();
        }
    }

    public void end() {
        if (spriteBatchInUse) {
            spriteBatch.end();
            spriteBatchInUse = false;
        }

        if (shapeRendererInUse) {
            shapeRenderer.end();
            shapeRendererInUse = false;
        }
    }

    public void clear(int width, int height) {
        end();
        stateStack.clear();
        if (this.width != width || this.height != height) {
            this.width = width;
            this.height = height;
            for (Pair<? extends ViewShader, ShaderProgram> shader : shaderLibrary.values()) {
                shader.getFirst().size(width, height);
            }
        }
        loadBlankState();
        Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_STENCIL_BUFFER_BIT);
        if (reloadShaders) {
            reloadShaders();
            reloadShaders = false;
            System.out.println("DEBUG: Shaders reloaded.");
        }
    }

    public void pushState() {
        stateStack.add(new State());
    }

    public void popState() {
        end();
        assert !stateStack.isEmpty();
        stateStack.remove(stateStack.size() - 1).load(this);
        camera.update();
        spriteBatch.setProjectionMatrix(camera.combined);
        shapeRenderer.setProjectionMatrix(camera.combined);
    }

    public void loadBlankState() {
        end();
        camera.setToOrtho(true, width, height);
        setColor(Color.WHITE);
        font = DEFAULT_FONT;
        spriteBatch.setProjectionMatrix(camera.combined);
        shapeRenderer.setProjectionMatrix(camera.combined);
    }

    public void translate(float x, float y) {
        end();
        camera.translate(-x, -y);

        camera.update();
        spriteBatch.setProjectionMatrix(camera.combined);
        shapeRenderer.setProjectionMatrix(camera.combined);
    }

    public static void debugReloadShaders() {
        reloadShaders = true;
    }

    /**
     * Rotate transformation matrix.
     *
     * @param angle in radians
     */
    public void rotate(float angle) {
        end();
        camera.rotate((float) Math.toDegrees(angle));
        camera.update();
        spriteBatch.setProjectionMatrix(camera.combined);
        shapeRenderer.setProjectionMatrix(camera.combined);
    }

    public void scale(float factor) {
        end();
        camera.zoom *= factor;
        camera.update();
        spriteBatch.setProjectionMatrix(camera.combined);
        shapeRenderer.setProjectionMatrix(camera.combined);
    }

    /**
     * Returns camera view of game screen.
     *
     * @return view of game screen with current transformations
     */
    public Polygon getCameraView() {
        Vector3[] corners = new Vector3[]{new Vector3(0, 0, 0),
                new Vector3(width, 0, 0),
                new Vector3(width, height, 0),
                new Vector3(0, height, 0)};

        for (Vector3 corner : corners) {
            camera.project(corner);
        }

        return new Polygon(new float[]{corners[0].x, corners[0].y,
                corners[1].x, corners[1].y,
                corners[2].x, corners[2].y,
                corners[3].x, corners[3].y});
    }

    public Vector3 windowToWorld(Vector3 vector) {
        camera.project(vector);
        return vector;
    }

    public Vector3 worldToWindow(Vector3 vector) {
        camera.unproject(vector);
        return vector;
    }

    public boolean isInView(float x, float y, float width, float height) {
        return getCameraView().getBoundingRectangle().overlaps(new Rectangle(x, y, width, height));
    }

    public float translateScreenX(float x) {
        Vector3 point = new Vector3(x, 0, 0);
        return point.x;
    }

    public float translateScreenY(float y) {
        Vector3 point = new Vector3(0, y, 0);
        return point.y;
    }

    public void draw(Texture texture, float x, float y, float originX, float originY, float width, float height, float rotation, int srcX, int srcY, int srcWidth, int srcHeight) {
        SpriteBatch batch = getSpriteBatch();
        activeShader.onDraw(this, batch, activeShaderProgram, texture);
        batch.draw(texture, x, y, originX, originY, width, height, 1.0f, 1.0f, (float) Math.toDegrees(rotation), srcX, srcY, srcWidth, srcHeight, false, true);
    }

    public Camera getCamera() {
        return camera;
    }

    private class State {
        private final Vector3 position, direction, up;
        private float zoom;
        private Color color;
        private BitmapFont font;

        public State() {
            this.position = new Vector3(camera.position);
            this.direction = new Vector3(camera.direction);
            this.up = new Vector3(camera.up);
            this.zoom = camera.zoom;
            this.color = View.this.color.cpy();
            this.font = View.this.font;
        }

        public void load(View view) {
            camera.position.set(position);
            camera.direction.set(direction);
            camera.up.set(up);
            camera.zoom = zoom;
            view.setColor(color);
            View.this.font = font;
        }
    }
}
