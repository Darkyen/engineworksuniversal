package darkyenus.engineworksuniversals.render.renderables;

import com.badlogic.gdx.graphics.Texture;
import darkyenus.engineworksuniversals.render.Renderable;
import darkyenus.engineworksuniversals.render.View;

/**
 * Private property.
 * User: Darkyen
 * Date: 4/8/13
 * Time: 5:21 PM
 */
@SuppressWarnings("UnusedDeclaration")
public abstract class Image implements Renderable {

    private Texture texture;
    private int x;
    private int y;
    private int width;
    private int height;
    private Image mask = null;

    protected Image() {
    }

    public Image(Image image) {
        this.texture = image.texture;
        this.x = image.x;
        this.y = image.y;
        this.width = image.width;
        this.height = image.height;
        this.mask = image.mask;
    }

    public Image(Texture texture) {
        this.texture = texture;
        x = 0;
        y = 0;
        width = texture.getWidth();
        height = texture.getHeight();
    }

    public Image(Texture texture, int x, int y, int width, int height) {
        this.texture = texture;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void render(View view, float x, float y) {
        render(view, x, y, width, height);
    }

    public void render(View view, float x, float y, float rotation) {
        render(view, x, y, width, height, rotation);
    }

    public void render(View view, float x, float y, float width, float height) {
        render(view, x, y, width, height, 0);
    }

    public void render(View view, float x, float y, float width, float height, float rotation) {
        render(view, x, y, width, height, rotation, 0, 0, this.width, this.height);
    }

    public void render(View view, float x, float y, float width, float height, int textureX, int textureY, int textureWidth, int textureHeight) {
        render(view, x, y, width, height, 0, textureX, textureY, textureWidth, textureHeight);
    }

    public void render(View view, float x, float y, float width, float height, float rotation, int textureX, int textureY, int textureWidth, int textureHeight) {
        renderImage(view, x, y, width, height, rotation, textureX, textureY, textureWidth, textureHeight);
    }

    protected abstract void renderImage(View view, float x, float y, float width, float height, float rotation, int textureX, int textureY, int textureWidth, int textureHeight);

    public void renderCentered(View view, float x, float y) {
        renderCentered(view, x, y, width, height);
    }

    public void renderCentered(View view, float x, float y, float rotation) {
        renderCentered(view, x, y, width, height, rotation);
    }

    public void renderCentered(View view, float x, float y, float width, float height) {
        renderCentered(view, x, y, width, height, 0);
    }

    public void render(float x, float y, float scale, float rotation, View view) {
        renderCentered(view, x, y, this.width * scale, this.height * scale, rotation);
    }

    public void renderCentered(View view, float x, float y, float width, float height, float rotation) {
        render(view, x - width / 2, y - height / 2, width, height, rotation);
    }

    //
    public void update(int delta) {
    }

    /**
     * Splits this image into smaller images.
     * When this texture's dimensions are not divisible by given splitting ones, surplus is omitted.
     * Resulting sprites inherit mask.
     *
     * @param spriteWidth  width of single result image on texture
     * @param spriteHeight height  of single result image on texture
     * @return array of split images
     */
    public Image[][] split(int spriteWidth, int spriteHeight) {
        int xAmountOfSprites = width / spriteWidth;
        int yAmountOfSprites = height / spriteHeight;

        Image[][] result = new Image[xAmountOfSprites][yAmountOfSprites];

        for (int x = 0; x < xAmountOfSprites; x++) {
            for (int y = 0; y < yAmountOfSprites; y++) {
                result[x][y] = new BasicImage(texture, this.x + x * spriteWidth, this.y + y * spriteHeight, spriteWidth, spriteHeight);
                if (mask != null) {
                    result[x][y].mask = new BasicImage(mask.texture, mask.x + x * spriteWidth, mask.y + y * spriteHeight, spriteWidth, spriteHeight);
                }
            }
        }

        return result;
    }

    public Texture getTexture() {
        return texture;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setMask(Image mask) {
        this.mask = mask;
    }

    public Image getMask() {
        return mask;
    }

    /**
     * To be used when loading.
     *
     * @param x coordinate on texture
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * To be used when loading.
     *
     * @param y coordinate on texture
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * To be used when loading.
     *
     * @param width on texture
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * To be used when loading.
     *
     * @param height on texture
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * Returns shallow copy of this image.
     * When implementing, make sure that you use Image#Image(Image) constructor,
     * otherwise you may forgot some attributes, such as mask.
     *
     * @return Shallow copy of this image.
     */
    public abstract Image duplicate();
}
