package darkyenus.engineworksuniversals.render.renderables;

import com.badlogic.gdx.graphics.Texture;
import darkyenus.engineworksuniversals.render.View;

/**
 * Private property.
 * User: Darkyen
 * Date: 4/22/13
 * Time: 11:43 AM
 */
public class BasicImage extends Image {

    public BasicImage(Texture texture) {
        super(texture);
    }

    public BasicImage(Texture texture, int x, int y, int width, int height) {
        super(texture, x, y, width, height);
    }

    public BasicImage(Image image) {
        super(image);
    }

    public void renderImage(View view, float x, float y, float width, float height, float rotation, int textureX, int textureY, int textureWidth, int textureHeight) {
        view.draw(getTexture(),//Texture
                x, y,//X,Y
                this.getX() + width / 2, this.getY() + height / 2,//OriginX OriginY
                width, height,//Width, Height
                rotation//Rotation
                , this.getX() + textureX, this.getY() + textureY,//Src X, Src Y
                textureWidth, textureHeight//Src Width, Src Height
        );
    }

    @Override
    public Image duplicate() {
        return new BasicImage(this);
    }
}
