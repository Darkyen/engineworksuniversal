package darkyenus.engineworksuniversals.render.renderables;

import com.badlogic.gdx.graphics.Color;
import darkyenus.engineworksuniversals.render.View;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/23/13
 * Time: 3:40 PM
 */
public class SolidImage extends Image {

    private Color color;

    public SolidImage(Image image, Color color) {
        super(image);
        this.color = color;
    }

    public SolidImage(int width, int height, Color color) {
        this.setWidth(width);
        this.setHeight(height);
        this.color = color;
    }

    public SolidImage(Color color) {
        this.color = color;
    }

    @Override
    protected void renderImage(View view, float x, float y, float width, float height, float rotation, int textureX, int textureY, int textureWidth, int textureHeight) {
        view.setColor(color);
        view.fillRect(x, y, width, height, rotation);
    }

    @Override
    public Image duplicate() {
        return new SolidImage(this, color);
    }
}
