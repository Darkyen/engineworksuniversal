package darkyenus.engineworksuniversals.render.renderables;

import com.badlogic.gdx.graphics.Texture;
import darkyenus.engineworksuniversals.render.View;

/**
 * Private property.
 * User: Darkyen & jIRKA
 * Date: 4/23/13
 * Time: 7:32 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class CorneredImage extends Image {
    public static final int LEFT_SIDE_WIDTH_INDEX = 0;
    public static final int RIGHT_SIDE_WIDTH_INDEX = 1;
    public static final int TOP_SIDE_HEIGHT_INDEX = 2;
    public static final int BOTTOM_SIDE_HEIGHT_INDEX = 3;
    public static final int DIMENSIONS_ARRAY_SIZE = 4;
    private int[] dimensions;

    public CorneredImage(Texture texture, int[] dimensions) {
        super(texture);
        this.dimensions = dimensions;
    }

    public CorneredImage(Texture texture, int texX, int texY, int texWidth, int texHeight, int[] dimensions) {
        super(texture, texX, texY, texWidth, texHeight);
        this.dimensions = dimensions;
    }

    public CorneredImage(Image image, int[] dimensions) {
        super(image.getTexture(), image.getX(), image.getY(), image.getWidth(), image.getHeight());
        this.dimensions = dimensions;
        if (image.getMask() != null) {
            setMask(new CorneredImage(image.getMask().getTexture(), image.getMask().getX(), image.getMask().getY(), image.getMask().getWidth(), image.getMask().getHeight(), dimensions));
        }
    }

    @Override
    public void renderImage(View view, float x, float y, float width, float height, float rotation, int textureX, int textureY, int textureWidth, int textureHeight) {
        //first render content, then sides and at last corners
        float tileSizeX = textureWidth - dimensions[LEFT_SIDE_WIDTH_INDEX] - dimensions[RIGHT_SIDE_WIDTH_INDEX];
        float tileSizeY = textureHeight - dimensions[TOP_SIDE_HEIGHT_INDEX] - dimensions[BOTTOM_SIDE_HEIGHT_INDEX];
        float pixelsToRenderX = width - dimensions[LEFT_SIDE_WIDTH_INDEX] - dimensions[RIGHT_SIDE_WIDTH_INDEX];
        float pixelsToRenderY = height - dimensions[TOP_SIDE_HEIGHT_INDEX] - dimensions[BOTTOM_SIDE_HEIGHT_INDEX];
        float atPixelX;
        float atPixelY = 0;
        while (atPixelY < pixelsToRenderY) {
            atPixelX = 0;
            while (atPixelX < pixelsToRenderX) {
                view.draw(getTexture(),
                        x + dimensions[LEFT_SIDE_WIDTH_INDEX] + atPixelX, y + dimensions[TOP_SIDE_HEIGHT_INDEX] + atPixelY, //X,Y
                        this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2,//OriginX OriginY
                        Math.min(tileSizeX, pixelsToRenderX - atPixelX), Math.min(tileSizeY, pixelsToRenderY - atPixelY), //Width, Height
                        rotation,//Rotation
                        this.getX() + textureX + dimensions[LEFT_SIDE_WIDTH_INDEX], this.getY() + textureY + dimensions[TOP_SIDE_HEIGHT_INDEX],//Src X, Src Y
                        Math.round(Math.min(tileSizeX, pixelsToRenderX - atPixelX)), Math.round(Math.min(tileSizeY, pixelsToRenderY - atPixelY)) //Src Width, Src Height
                );
                atPixelX += tileSizeX;
            }
            atPixelY += tileSizeY;
        }
        float tileSize;
        float pixelsToRender;
        float atPixel;
        //top side
        tileSize = textureWidth - dimensions[LEFT_SIDE_WIDTH_INDEX] - dimensions[RIGHT_SIDE_WIDTH_INDEX];
        pixelsToRender = width - dimensions[LEFT_SIDE_WIDTH_INDEX] - dimensions[RIGHT_SIDE_WIDTH_INDEX];
        atPixel = 0;
        while (atPixel < pixelsToRender) {
            view.draw(getTexture(),//Texture
                    x + dimensions[LEFT_SIDE_WIDTH_INDEX] + atPixel, y,
                    this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2,//OriginX OriginY
                    Math.min(tileSize, pixelsToRender - atPixel), dimensions[TOP_SIDE_HEIGHT_INDEX],//Width, Height
                    rotation//Rotation
                    , this.getX() + textureX + dimensions[LEFT_SIDE_WIDTH_INDEX], this.getY() + textureY,//Src X, Src Y
                    Math.round(Math.min(tileSize, pixelsToRender - atPixel)), dimensions[TOP_SIDE_HEIGHT_INDEX] //Src Width, Src Height
            );
            atPixel += tileSize;
        }

        //bottom side
        tileSize = textureWidth - dimensions[LEFT_SIDE_WIDTH_INDEX] - dimensions[RIGHT_SIDE_WIDTH_INDEX];
        pixelsToRender = width - dimensions[LEFT_SIDE_WIDTH_INDEX] - dimensions[RIGHT_SIDE_WIDTH_INDEX];
        atPixel = 0;
        while (atPixel < pixelsToRender) {
            view.draw(getTexture(),//Texture
                    x + dimensions[LEFT_SIDE_WIDTH_INDEX] + atPixel, y + height - dimensions[BOTTOM_SIDE_HEIGHT_INDEX],//X,Y
                    this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2,//OriginX OriginY
                    Math.min(tileSize, pixelsToRender - atPixel), dimensions[BOTTOM_SIDE_HEIGHT_INDEX],//Width, Height
                    rotation//Rotation
                    , this.getX() + textureX + dimensions[LEFT_SIDE_WIDTH_INDEX], this.getY() + textureY + textureHeight - dimensions[BOTTOM_SIDE_HEIGHT_INDEX],//Src X, Src Y
                    Math.round(Math.min(tileSize, pixelsToRender - atPixel)), dimensions[BOTTOM_SIDE_HEIGHT_INDEX] //Src Width, Src Height
            );
            atPixel += tileSize;
        }

        //left side
        tileSize = textureHeight - dimensions[TOP_SIDE_HEIGHT_INDEX] - dimensions[BOTTOM_SIDE_HEIGHT_INDEX];
        pixelsToRender = height - dimensions[TOP_SIDE_HEIGHT_INDEX] - dimensions[BOTTOM_SIDE_HEIGHT_INDEX];
        atPixel = 0;
        while (atPixel < pixelsToRender) {
            view.draw(getTexture(),//Texture
                    x, y + dimensions[TOP_SIDE_HEIGHT_INDEX] + atPixel,//X,Y
                    this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2,//OriginX OriginY
                    dimensions[LEFT_SIDE_WIDTH_INDEX], Math.min(tileSize, pixelsToRender - atPixel),//Width, Height
                    rotation//Rotation
                    , this.getX() + textureX, this.getY() + textureY + dimensions[TOP_SIDE_HEIGHT_INDEX],//Src X, Src Y
                    dimensions[LEFT_SIDE_WIDTH_INDEX], Math.round(Math.min(tileSize, pixelsToRender - atPixel)) //Src Width, Src Height
            );
            atPixel += tileSize;
        }
        //right side
        tileSize = textureHeight - dimensions[TOP_SIDE_HEIGHT_INDEX] - dimensions[BOTTOM_SIDE_HEIGHT_INDEX];
        pixelsToRender = height - dimensions[TOP_SIDE_HEIGHT_INDEX] - dimensions[BOTTOM_SIDE_HEIGHT_INDEX];
        atPixel = 0;
        while (atPixel < pixelsToRender) {
            view.draw(getTexture(),//Texture
                    x + width - dimensions[RIGHT_SIDE_WIDTH_INDEX], y + dimensions[TOP_SIDE_HEIGHT_INDEX] + atPixel,//X,Y
                    this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2,//OriginX OriginY
                    dimensions[RIGHT_SIDE_WIDTH_INDEX], Math.min(tileSize, pixelsToRender - atPixel),//Width, Height
                    rotation//Rotation
                    , this.getX() + textureX + textureWidth - dimensions[RIGHT_SIDE_WIDTH_INDEX], this.getY() + textureY + dimensions[TOP_SIDE_HEIGHT_INDEX],//Src X, Src Y
                    dimensions[RIGHT_SIDE_WIDTH_INDEX], Math.round(Math.min(tileSize, pixelsToRender - atPixel)) //Src Width, Src Height
            );
            atPixel += tileSize;
        }

        //left top corner
        view.draw(getTexture(),//Texture
                x, y,//X,Y
                this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2,//OriginX OriginY
                dimensions[LEFT_SIDE_WIDTH_INDEX], dimensions[TOP_SIDE_HEIGHT_INDEX],//Width, Height
                rotation//Rotation
                , this.getX() + textureX, this.getY() + textureY,//Src X, Src Y
                dimensions[LEFT_SIDE_WIDTH_INDEX], dimensions[TOP_SIDE_HEIGHT_INDEX]//Src Width, Src Height
        );
        //right top corner
        view.draw(getTexture(),//Texture
                x + width - dimensions[RIGHT_SIDE_WIDTH_INDEX], y,//X,Y
                this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2,//OriginX OriginY
                dimensions[RIGHT_SIDE_WIDTH_INDEX], dimensions[TOP_SIDE_HEIGHT_INDEX],//Width, Height
                rotation//Rotation
                , this.getX() + textureX + textureWidth - dimensions[RIGHT_SIDE_WIDTH_INDEX], this.getY() + textureY,//Src X, Src Y
                dimensions[RIGHT_SIDE_WIDTH_INDEX], dimensions[TOP_SIDE_HEIGHT_INDEX]//Src Width, Src Height
        );
        //left bottom corner
        view.draw(getTexture(),//Texture
                x, y + height - dimensions[BOTTOM_SIDE_HEIGHT_INDEX],//X,Y
                this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2,//OriginX OriginY
                dimensions[LEFT_SIDE_WIDTH_INDEX], dimensions[BOTTOM_SIDE_HEIGHT_INDEX],//Width, Height
                rotation//Rotation
                , this.getX() + textureX, this.getY() + textureY + textureHeight - dimensions[BOTTOM_SIDE_HEIGHT_INDEX],//Src X, Src Y
                dimensions[LEFT_SIDE_WIDTH_INDEX], dimensions[BOTTOM_SIDE_HEIGHT_INDEX]//Src Width, Src Height
        );
        //right bottom corner
        view.draw(getTexture(),//Texture
                x + width - dimensions[RIGHT_SIDE_WIDTH_INDEX], y + height - dimensions[BOTTOM_SIDE_HEIGHT_INDEX],//X,Y
                this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2,//OriginX OriginY
                dimensions[RIGHT_SIDE_WIDTH_INDEX], dimensions[BOTTOM_SIDE_HEIGHT_INDEX],//Width, Height
                rotation//Rotation
                , this.getX() + textureX + textureWidth - dimensions[RIGHT_SIDE_WIDTH_INDEX], this.getY() + textureY + textureHeight - dimensions[BOTTOM_SIDE_HEIGHT_INDEX],//Src X, Src Y
                dimensions[RIGHT_SIDE_WIDTH_INDEX], dimensions[BOTTOM_SIDE_HEIGHT_INDEX]//Src Width, Src Height
        );

    }

    @Override
    public Image duplicate() {
        return new CorneredImage(this, dimensions);
    }
}