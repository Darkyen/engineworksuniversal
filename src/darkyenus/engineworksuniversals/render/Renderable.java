package darkyenus.engineworksuniversals.render;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/24/13
 * Time: 8:38 AM
 */
public interface Renderable {
    public void render(float x, float y, float scale, float rotation, View view);
    public void update(int delta);
}
