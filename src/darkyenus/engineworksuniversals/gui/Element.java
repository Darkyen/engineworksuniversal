package darkyenus.engineworksuniversals.gui;

import com.badlogic.gdx.graphics.Color;
import darkyenus.engineworksuniversals.geometry.Dimension;
import darkyenus.engineworksuniversals.geometry.Rectangle;
import darkyenus.engineworksuniversals.render.View;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Darkyen
 */
@SuppressWarnings({"UnusedParameters", "UnusedDeclaration"})
public abstract class Element implements Component {

    protected Component parent;
    protected Rectangle boundingBox;
    protected ArrayList<Component> components = new ArrayList<Component>();
    protected ArrayList<Style> customStyles = new ArrayList<Style>();
    protected Component focused;
    protected Style style;
    private Dimension screenDimension;
    private boolean focusable = true;
    private int preferredWidth;
    private int preferredHeight;
    private Color tint = null;
    private boolean visible = true;


    public Element(Component parent, Rectangle boundingBox) {
        this(parent, boundingBox, parent.getStyle());
    }

    public Element(Component parent, Rectangle boundingBox, Style style) {
        this.parent = parent;
        this.boundingBox = boundingBox;
        this.style = style;
        this.screenDimension = parent.getDimension();
        this.preferredWidth = (int) boundingBox.getWidth(getDimension());
        this.preferredHeight = (int) boundingBox.getHeight(getDimension());
    }

    protected Element() {
        this.boundingBox = new Rectangle.AbsoluteRectangle(0, 0, 0, 0);
    }

    @Override
    public Component getParent() {
        return parent;
    }

    @Override
    public void addComponent(Component component) {
        addComponent(component, style);
    }

    @Override
    public void addComponent(Component component, Style alternateStyle) {
        components.add(component);
        customStyles.add(alternateStyle);
        if (alternateStyle != null) {
            component.setBorder(alternateStyle.createIdealBorderRectangle(component));
        }

        float xAccordingToNewComponent = getBorder().getX(getDimension()) + component.getBorder().getX(getDimension()) - style.getXOffset();
        float yAccordingToNewComponent = getBorder().getY(getDimension()) + component.getBorder().getY(getDimension()) - style.getYOffset();
        getBorder().setX(getDimension(), Math.min(getBorder().getX(getDimension()), xAccordingToNewComponent));
        getBorder().setY(getDimension(), Math.min(getBorder().getY(getDimension()), yAccordingToNewComponent));

        float widthAccordingToNewComponent = (component.getBorder().getX(getDimension()) + component.getBorder().getWidth(getDimension())) - style.getXOffset();
        float heightAccordingToNewComponent = (component.getBorder().getY(getDimension()) + component.getBorder().getHeight(getDimension())) - style.getYOffset();
        getBorder().setWidth(getDimension(), Math.max(getBorder().getWidth(getDimension()), widthAccordingToNewComponent));
        getBorder().setHeight(getDimension(), Math.max(getBorder().getHeight(getDimension()), heightAccordingToNewComponent));
    }

    @Override
    public void restyle() {
        ArrayList<Component> componentList = new ArrayList<Component>(components);
        ArrayList<Style> styleList = new ArrayList<Style>(customStyles);
        components.clear();
        customStyles.clear();

        Iterator<Component> componentsIterator = componentList.iterator();
        Iterator<Style> styleIterator = styleList.iterator();

        while (componentsIterator.hasNext()) {
            Component toRestyle = componentsIterator.next();
            Style toRestyleWith = styleIterator.next();
            addComponent(toRestyle, toRestyleWith);
            toRestyle.restyle();
        }
    }

    public Dimension getDimension() {
        return screenDimension;
    }

    @Override
    public List<Component> getComponents() {
        return components;
    }

    public void clearComponents() {
        components.clear();
        customStyles.clear();
        focused = null;
    }

    @Override
    public Rectangle getBorder() {
        return boundingBox;
    }

    @Override
    public void setBorder(Rectangle border) {
        this.boundingBox = border;
    }

    @Override
    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    @Override
    public void render(View context, boolean focused) {
        if (visible) {
            context.pushState();
            context.translate(getBorder().getX(getDimension()), getBorder().getY(getDimension()));
            if (tint != null) {
                context.setColor(tint);
            }
            context.pushState();
            renderComponent(context, focused && this.focused == null);
            context.popState();
            for (Component component : components) {
                component.render(context, component.equals(this.focused));
            }
            context.popState();
        }
    }

    public abstract void renderComponent(View context, boolean focused);

    @Override
    public void update(int delta, boolean focused) {
        if (visible) {
            for (Component component : components) {
                component.update(delta, component.equals(this.focused));
            }
        }
    }

    public void checkFocusOn(int x, int y) {
        focused = null;
        for (Component component : components) {
            if (component.getBorder().contains(getDimension(), x, y) && component.isFocusable()) {
                focused = component;
            }
        }
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
        if (!visible) {
            getParent().defocus(this);
        }
    }

    //<editor-fold defaultState="collapsed" desc="Key Listeners">
    @Override
    public boolean componentKeyPressed(int key) {
        if (!visible) return false;
        if (focused != null) {
            return focused.componentKeyPressed(key);
        } else {
            return localKeyPressed(key);
        }
    }

    @Override
    public boolean componentKeyReleased(int key) {
        if (!visible) return false;
        if (focused != null) {
            return focused.componentKeyReleased(key);
        } else {
            return localKeyReleased(key);
        }
    }

    @Override
    public boolean componentKeyTyped(char c) {
        if (!visible) return false;
        if (focused != null) {
            return focused.componentKeyTyped(c);
        } else {
            return localKeyTyped(c);
        }
    }
    //</editor-fold>

    @Override
    public boolean componentMouseMoved(int newX, int newY) {
        if (!visible) return false;
        for (Component component : components) {
            component.componentMouseMoved(newX - (int) getBorder().getX(getDimension()), newY - (int) getBorder().getY(getDimension()));
        }
        return localMouseMoved(newX, newY, focused == null);
    }

    @Override
    public boolean componentMouseDragged(int newX, int newY) {
        if (!visible) return false;
        for (Component component : components) {
            component.componentMouseDragged(newX - (int) getBorder().getX(getDimension()), newY - (int) getBorder().getY(getDimension()));
        }
        return localMouseDragged(newX, newY, focused == null);
    }

    @Override
    public boolean componentMousePressed(int button, int x, int y) {
        if (!visible) return false;
        checkFocusOn(x, y);
        if (focused != null) {
            focused.componentMousePressed(button, x - (int) focused.getBorder().getX(getDimension()), y - (int) focused.getBorder().getY(getDimension()));
            return true;
        } else {
            return localMousePressed(button, x, y);
        }
    }

    @Override
    public boolean componentMouseReleased(int button, int x, int y) {
        if (!visible) return false;
        checkFocusOn(x, y);
        if (focused != null) {//May not work?
            for (Component component : components) {
                component.componentMouseReleased(button, x - (int) focused.getBorder().getX(getDimension()), y - (int) focused.getBorder().getY(getDimension()));
            }
        }
        localMouseReleased(button, x, y, focused == null);
        return true;
    }

    @Override
    public boolean componentMouseWheelMoved(int change) {
        if (!visible) return false;
        if (focused != null) {
            return focused.componentMouseWheelMoved(change);
        } else {
            return localMouseWheelMoved(change);
        }
    }

    @Override
    public int getPreferredWidth() {
        return preferredWidth;
    }

    @Override
    public int getPreferredHeight() {
        return preferredHeight;
    }

    public Color getTint() {
        return tint;
    }

    public void setTint(Color tint) {
        this.tint = tint;
    }

    //COMPONENT OVERRIDABLE CALLS
    public boolean localKeyPressed(int key) {
        return false;
    }

    public boolean localKeyReleased(int key) {
        return false;
    }

    public boolean localKeyTyped(char c) {
        return false;
    }

    public boolean localMouseMoved(int newX, int newY, boolean focused) {
        return false;
    }

    public boolean localMouseDragged(int newX, int newY, boolean focused) {
        return false;
    }

    public boolean localMousePressed(int button, int x, int y) {
        return false;
    }

    public boolean localMouseReleased(int button, int x, int y, boolean focused) {
        return false;
    }

    public boolean localMouseWheelMoved(int change) {
        return false;
    }

    /**
     * @return the focusable
     */
    @Override
    public boolean isFocusable() {
        return focusable;
    }

    /**
     * @param focusable the focusable to set
     */
    public void setFocusable(boolean focusable) {
        this.focusable = focusable;
    }

    public void requestFocus() {
        if (visible) {
            focused = null;
            getParent().focus(this);
        } else {
            getParent().requestFocus();
        }
    }

    public void focus(Component child) {
        if (this.getComponents().contains(child)) {
            focused = child;
        }
        if (getParent() != null) {
            getParent().focus(this);
        }
    }

    public void defocus(Component child) {
        if (child.equals(focused)) {
            focused = null;
        }
    }
}
