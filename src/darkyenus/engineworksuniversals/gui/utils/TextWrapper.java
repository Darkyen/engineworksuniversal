package darkyenus.engineworksuniversals.gui.utils;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import darkyenus.engineworksuniversals.render.View;

import java.util.ArrayList;

/**
 * Slick utility class that parses and renders given text into word-like block.
 * Block is aligned to left. (I think)
 * Is immutable.
 *
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class TextWrapper {
    private ArrayList<PositionedText> lines = new ArrayList<PositionedText>();
    private BitmapFont font;

    /**
     * Create TextWrapper with given parameters.
     *
     * @param text  text to format and render
     * @param font  font to render text with
     * @param width width of block, in pixels. Height is determined by amount of text.
     */
    public TextWrapper(String text, BitmapFont font, int width) {
        this.font = font;

        int position = 0;
        int lineHeight = (int) Math.floor(font.getLineHeight() + 0.5);
        int ySoFar = 0;
        while (position != text.length()) {
            int fits = font.computeVisibleGlyphs(text, position, text.length(), width);
            PositionedText line = new PositionedText(text.substring(position, position + fits), 0, ySoFar);
            lines.add(line);
            position += fits;
            ySoFar += lineHeight;
        }
    }

    public void render(View view, int x, int y) {
        view.pushState();
        view.translate(x, y);
        for (PositionedText text : lines) {
            text.render(view.getSpriteBatch());
        }
        view.popState();
    }

    private class PositionedText {
        private String text;
        private int x;
        private int y;

        public PositionedText(String text, int x, int y) {
            this.text = text;
            this.x = x;
            this.y = y;
        }

        public void render(SpriteBatch spriteBatch) {
            font.draw(spriteBatch, text, x, y);
        }
    }
}
