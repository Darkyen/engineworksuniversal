package darkyenus.engineworksuniversals.gui;

import darkyenus.engineworksuniversals.geometry.Dimension;
import darkyenus.engineworksuniversals.geometry.Rectangle;
import darkyenus.engineworksuniversals.render.View;

import java.util.List;

/**
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public interface Component {

    public Component getParent();

    public void addComponent(Component component);

    /**
     * Adds component and modifies its border with given Style.
     *
     * @param component        to modify
     * @param alternativeStyle to modify with, can be null to leave border alone
     */
    public void addComponent(Component component, Style alternativeStyle);

    /**
     * Get list of all components attached to the component
     *
     * @return list of components, never null
     */
    public List<Component> getComponents();

    public void clearComponents();

    public Dimension getDimension();

    /**
     * Get position relative to the parent.
     *
     * @return relative position
     */
    public Rectangle getBorder();

    public boolean isFocusable();

    public void setBorder(Rectangle border);

    public int getPreferredWidth();

    public int getPreferredHeight();

    public void render(View context, boolean focused);

    public void update(int delta, boolean focused);

    public void restyle();

    public Style getStyle();

    public void requestFocus();

    public void focus(Component child);

    public void defocus(Component child);

    //INPUT
    public boolean componentKeyPressed(int key);

    public boolean componentKeyReleased(int key);

    public boolean componentKeyTyped(char c);

    public boolean componentMouseMoved(int newX, int newY);

    public boolean componentMouseDragged(int newX, int newY);

    public boolean componentMousePressed(int button, int x, int y);

    public boolean componentMouseReleased(int button, int x, int y);

    public boolean componentMouseWheelMoved(int change);
}
