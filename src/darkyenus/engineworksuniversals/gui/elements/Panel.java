package darkyenus.engineworksuniversals.gui.elements;

import darkyenus.engineworksuniversals.geometry.Rectangle;
import darkyenus.engineworksuniversals.gui.Component;
import darkyenus.engineworksuniversals.gui.Element;
import darkyenus.engineworksuniversals.gui.Style;
import darkyenus.engineworksuniversals.render.View;
import darkyenus.engineworksuniversals.render.renderables.Image;

/**
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class Panel extends Element {

    private Image image;

    public Panel(Component parent, int x, int y, int width, int height, Image image) {
        super(parent, new Rectangle.RelativeRectangle(parent.getDimension(), x, y, width, height));
        this.image = image;
    }

    public Panel(Component parent, Style style, int x, int y, int width, int height, Image image) {
        super(parent, new Rectangle.RelativeRectangle(parent.getDimension(), x, y, width, height), style);
        this.image = image;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public void renderComponent(View context, boolean focused) {
        image.render(context, 0, 0, getBorder().getWidth(getDimension()), getBorder().getHeight(getDimension()));
    }
}
