package darkyenus.engineworksuniversals.gui.elements;

import darkyenus.engineworksuniversals.geometry.Rectangle;
import darkyenus.engineworksuniversals.gui.Component;
import darkyenus.engineworksuniversals.gui.Element;
import darkyenus.engineworksuniversals.render.View;

/**
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class Label extends Element {

    private String text;

    public Label(Component parent, int x, int y, String text) {
        super(parent, new Rectangle.RelativeRectangle(parent.getDimension(), x, y, text != null ? parent.getStyle().getFont().getBounds(text).width : 0, text != null ? parent.getStyle().getFont().getLineHeight() * 2 : 0));
        this.text = text;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
        this.getParent().restyle();
    }

    @Override
    public void renderComponent(View context, boolean focused) {
        context.setFont(getStyle().getFont());
        context.multiplyColor(getStyle().getTextColor());
        context.drawStringCentered(getText(), getBorder().getX(getDimension()) + getBorder().getWidth(getDimension()) / 2, getBorder().getY(getDimension()) + getBorder().getHeight(getDimension()) / 2);
    }
}
