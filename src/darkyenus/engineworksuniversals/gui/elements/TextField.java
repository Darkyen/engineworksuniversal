package darkyenus.engineworksuniversals.gui.elements;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.utils.TimeUtils;
import darkyenus.engineworksuniversals.gui.Component;
import darkyenus.engineworksuniversals.render.View;
import darkyenus.engineworksuniversals.render.renderables.Image;
import darkyenus.engineworksuniversals.render.viewshader.BufferMaskViewShader;
import darkyenus.engineworksuniversals.render.viewshader.DefaultViewShader;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * This Text Field makes this assertions: - There is a caret glyph of code/id 31
 * (if there is not, nothing is displayed)
 * <p/>
 * Notes: - Caret implementation makes a mess in kernings, do not use them or
 * exclude caret from calculations. (It is not bad, but it does not look
 * perfect)
 *
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class TextField extends Panel {
    private static final char CARET = 31;

    private StringBuilder text = new StringBuilder();
    private String tipText = "";
    private Pattern filter;
    private int caretPosition = 0;
    private long lastEdit = 0;
    private ArrayList<TextFieldListener> listeners = new ArrayList<TextFieldListener>();
    private float textStart = 10;
    private float maxCaretOffset = 10;

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public TextField(Component parent, int x, int y, int width, int height, Image image) {
        super(parent, x, y, width, height, image);
        setFilter("[\\u0020-\\u007E]");
    }

    public TextField(Component parent, int x, int y, int width, int height, Image image, String tipText) {
        this(parent, x, y, width, height, image);
        this.tipText = tipText;
    }

    @Override
    public void renderComponent(View view, boolean focused) {
        super.renderComponent(view, focused);

        BufferMaskViewShader maskViewShader = view.useShader(BufferMaskViewShader.class);

        //Draw Mask
        maskViewShader.beginMasking();
        getImage().getMask().render(view, 0, 0, getBorder().getWidth(getDimension()), getBorder().getHeight(getDimension()));
        view.flush();
        maskViewShader.endMasking();

        //Draw text into mask
        BitmapFont font = getStyle().getFont();
        view.setFont(font);
        view.multiplyColor(getStyle().getTextColor());

        String toRender = getRenderTextWithCaret(focused);
        BitmapFont.TextBounds renderedTextBoundsStartToCaret = font.getBounds(toRender, 0, Math.min(caretPosition, toRender.length()));

        float x = textStart;
        float maxCaretX = getBorder().getWidth(view) - maxCaretOffset;
        if (textStart + renderedTextBoundsStartToCaret.width > maxCaretX) {
            x = -renderedTextBoundsStartToCaret.width + maxCaretX;
        }
        float y = getBorder().getHeight(view) / 2 - font.getLineHeight() / 2;

        view.drawString(toRender, x, y, false);

        //Reset shader
        view.useShader(DefaultViewShader.class);
    }

    protected String getRenderTextWithCaret(boolean focused) {
        StringBuilder textWithCaret = new StringBuilder(getRenderText());
        if (focused && isTimeForCaret()) {
            textWithCaret.insert(Math.min(caretPosition, textWithCaret.length()), CARET);
        }
        return textWithCaret.toString();
    }

    private boolean isTimeForCaret() {
        long now = TimeUtils.millis();
        return (now / 600) % 2 == 1 //Toggle blink every 600 ms
                || now - 1000 < lastEdit;//Time for caret is true always if since last edit elapsed less than second
    }

    protected String getRenderText() {
        if (getText().isEmpty()) {
            return tipText;
        } else {
            return getText();
        }
    }

    /**
     * @return the text
     */
    public String getText() {
        return text.toString();
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text.setLength(0);
        this.text.append(text);
        this.caretPosition = text.length();
    }

    public void clearText() {
        this.text.setLength(0);
        this.caretPosition = text.length();
    }

    /**
     * @return the filter
     */
    public String getFilter() {
        return filter.pattern();
    }

    /**
     * @param filter the filter to set
     */
    public void setFilter(String filter) {
        this.filter = Pattern.compile(filter);
    }

    public float getTextStart() {
        return textStart;
    }

    public void setTextStart(float textStart) {
        this.textStart = textStart;
    }

    public float getMaxCaretOffset() {
        return maxCaretOffset;
    }

    public void setMaxCaretOffset(float maxCaretOffset) {
        this.maxCaretOffset = maxCaretOffset;
    }

    //Input
    @Override
    public boolean localKeyPressed(int key) {
        if (key == Input.Keys.LEFT) {
            caretPosition--;
            if (caretPosition < 0) {
                caretPosition = 0;
            }
            lastEdit = TimeUtils.millis();
        } else if (key == Input.Keys.RIGHT) {
            if (caretPosition < text.length()) {
                caretPosition++;
            }
            lastEdit = TimeUtils.millis();
        } else if (key == Input.Keys.DEL) {
            if (caretPosition > 0) {
                text.deleteCharAt(caretPosition - 1);
                caretPosition--;
            }
            lastEdit = TimeUtils.millis();
        } else if (key == Input.Keys.FORWARD_DEL) {
            if (caretPosition < text.length()) {
                text.deleteCharAt(caretPosition);
            }
            lastEdit = TimeUtils.millis();
        } else if (key == Input.Keys.ENTER) {
            for (TextFieldListener listener : listeners) {
                listener.onEnter(this);
            }
        }
        return true;
    }

    @Override
    public boolean localKeyTyped(char c) {
        if (filter.matcher(Character.toString(c)).matches()) {
            text.insert(caretPosition, c);
            caretPosition++;
            lastEdit = TimeUtils.millis();
        }
        return true;
    }

    public void addListener(TextFieldListener listener) {
        listeners.add(listener);
    }

    public static interface TextFieldListener {
        public void onEnter(TextField textField);
    }
}
