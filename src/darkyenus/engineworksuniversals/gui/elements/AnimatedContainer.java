package darkyenus.engineworksuniversals.gui.elements;

import com.badlogic.gdx.graphics.Color;
import darkyenus.engineworksuniversals.geometry.Rectangle;
import darkyenus.engineworksuniversals.gui.Component;
import darkyenus.engineworksuniversals.gui.Element;
import darkyenus.engineworksuniversals.render.View;
import darkyenus.engineworksuniversals.tween.Tween;

/**
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class AnimatedContainer extends Element {

    public static final Tween FINISHED_CONTROLLER = new Tween() {

        @Override
        public float get(float time) {
            return 1;
        }
    };

    private int timeMax;
    private AnimationListener listener;
    private int time = 0;//Progress on animation, timeMax means finished
    private int timeShift = 0;

    //Location
    private int fromX;
    private int fromY;
    private int toX;
    private int toY;
    private Tween movementController;

    //Color
    private Tween redController;
    private Tween greenController;
    private Tween blueController;
    private Tween alphaController;
    private Color startColor;
    private Color endColor;
    private Color overlayColor;

    //Everything
    public AnimatedContainer(Component parent, int fromX, int fromY, int toX, int toY, Tween controller, Color startColor, Color endColor, Tween redController, Tween greenController, Tween blueController, Tween alphaController, int time) {
        super(parent, new Rectangle.RelativeRectangle(parent.getDimension(), fromX, fromY, 0, 0));
        this.timeMax = time;
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        this.movementController = controller;

        this.startColor = startColor;
        this.endColor = endColor;
        this.overlayColor = new Color(startColor);
        this.redController = redController != null ? redController : FINISHED_CONTROLLER;
        this.greenController = greenController != null ? greenController : FINISHED_CONTROLLER;
        this.blueController = blueController != null ? blueController : FINISHED_CONTROLLER;
        this.alphaController = alphaController != null ? alphaController : FINISHED_CONTROLLER;
    }

    //Movement only
    public AnimatedContainer(Component parent, int fromX, int fromY, int toX, int toY, Tween controller, int time) {
        this(parent, fromX, fromY, toX, toY, controller, new Color(0f, 0f, 0f, 0f), new Color(0f, 0f, 0f, 0f), Tween.LINEAR, Tween.LINEAR, Tween.LINEAR, Tween.LINEAR, time);
    }

    //Colors only
    public AnimatedContainer(Component parent, Color startColor, Color endColor, Tween redController, Tween greenController, Tween blueController, Tween alphaController, int time) {
        this(parent, 0, 0, 0, 0, Tween.LINEAR, startColor, endColor, redController, greenController, blueController, alphaController, time);
    }

    public void startAnimation(AnimationListener animationListener, boolean reverse) {
        this.listener = animationListener;
        timeShift = reverse ? -1 : 1;
    }

    @Override
    public void renderComponent(View context, boolean focused) {
        context.setColor(overlayColor);
        context.fillRect(0, 0, getBorder().getWidth(getDimension()), getBorder().getHeight(getDimension()));
        context.setColor(Color.WHITE);
    }

    @Override
    public void update(int delta, boolean focused) {
        time += delta * timeShift;
        if (time < 0) {
            time = 0;
            timeShift = 0;
            if (listener != null) {
                listener.onAnimationDone(this);
                listener = null;
            }
        } else if (time > timeMax) {
            time = timeMax;
            timeShift = 0;
            if (listener != null) {
                listener.onAnimationDone(this);
                listener = null;
            }
        }

        float animationTime = (float) time / (float) timeMax;
        getBorder().setX(getDimension(), (toX - fromX) * movementController.get(animationTime) + fromX);
        getBorder().setY(getDimension(), (toY - fromY) * movementController.get(animationTime) + fromY);

        overlayColor.r = (endColor.r - startColor.r) * redController.get(animationTime) + startColor.r;
        overlayColor.g = (endColor.g - startColor.g) * greenController.get(animationTime) + startColor.g;
        overlayColor.b = (endColor.b - startColor.b) * blueController.get(animationTime) + startColor.b;
        overlayColor.a = (endColor.a - startColor.a) * alphaController.get(animationTime) + startColor.a;
    }

    public interface AnimationListener {
        public void onAnimationDone(AnimatedContainer piece);
    }
}
