package darkyenus.engineworksuniversals.gui.elements;

import com.badlogic.gdx.Input;
import darkyenus.engineworksuniversals.gui.Component;
import darkyenus.engineworksuniversals.render.View;
import darkyenus.engineworksuniversals.render.renderables.Image;

import java.util.ArrayList;

/**
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class Button extends Panel {

    public static final int INDEX_IDLE = 0;
    public static final int INDEX_HOT = 1;
    public static final int INDEX_ACTIVE = 2;

    private boolean pressed;
    private boolean selected;
    private ArrayList<ButtonListener> listeners = new ArrayList<ButtonListener>();
    private String text;
    private Image[] images;

    public Button(Component parent, int x, int y, int width, int height, Image[] images, String text) {
        super(parent, x, y, width, height, images[0]);
        this.text = text;
        this.images = images;
    }

    @Override
    public boolean localMousePressed(int button, int x, int y) {
        if (button == Input.Buttons.LEFT) {
            pressed = true;
        }
        return true;
    }

    @Override
    public boolean localMouseReleased(int button, int x, int y, boolean focused) {
        if (focused && pressed) {
            pressed = false;
            for (ButtonListener listener : listeners) {
                listener.onButtonPressed(this);
            }
        }
        return true;
    }

    @Override
    public boolean localMouseMoved(int newX, int newY, boolean focused) {
        selected = getBorder().contains(getDimension(), newX, newY);
        return true;
    }

    public void addButtonListener(ButtonListener listener) {
        listeners.add(listener);
    }

    @Override
    public void renderComponent(View context, boolean focused) {
        if (pressed) {
            setImage(images[INDEX_ACTIVE]);
        } else if (selected) {
            setImage(images[INDEX_HOT]);
        } else {
            setImage(images[INDEX_IDLE]);
        }

        super.renderComponent(context, focused);
        context.setFont(getStyle().getFont());
        context.setColor(getStyle().getTextColor());
        context.drawStringCentered(text, getBorder().getWidth(getDimension()) / 2f, getBorder().getHeight(getDimension()) / 2f);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public interface ButtonListener {
        public void onButtonPressed(Button from);
    }
}
