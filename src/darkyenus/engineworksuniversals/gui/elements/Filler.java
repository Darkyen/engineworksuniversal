package darkyenus.engineworksuniversals.gui.elements;


import darkyenus.engineworksuniversals.geometry.Rectangle;
import darkyenus.engineworksuniversals.gui.Component;
import darkyenus.engineworksuniversals.gui.Element;
import darkyenus.engineworksuniversals.gui.Style;
import darkyenus.engineworksuniversals.render.View;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/23/13
 * Time: 3:32 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class Filler extends Element {

    public Filler(Component parent, Rectangle boundingBox) {
        super(parent, boundingBox);
    }

    public Filler(Component parent, Rectangle boundingBox, Style style) {
        super(parent, boundingBox, style);
    }

    public Filler(Component parent, float x, float y, float width, float height) {
        super(parent, new Rectangle.AbsoluteRectangle(x, y, width, height));
    }

    public Filler(Component parent, float x, float y, float width, float height, Style style) {
        super(parent, new Rectangle.AbsoluteRectangle(x, y, width, height), style);
    }

    @Override
    public void renderComponent(View context, boolean focused) {
    }
}
