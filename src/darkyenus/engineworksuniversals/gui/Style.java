package darkyenus.engineworksuniversals.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import darkyenus.engineworksuniversals.geometry.Rectangle;
import darkyenus.engineworksuniversals.render.ResourceManager;
import darkyenus.engineworksuniversals.render.View;

/**
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class Style {

    public static Style createStyle(BitmapFont font, LayoutManager manager) {
        return new Style(2, 2, Color.BLACK, font, manager);
    }

    public static Style createStyle(LayoutManager manager) {
        return new Style(2, 2, Color.BLACK, View.DEFAULT_FONT, manager);
    }

    public static Style createStyle(Style style, LayoutManager manager) {
        return new Style(style.getXOffset(), style.getYOffset(), style.getTextColor(), style.getFont(), manager);
    }

    private int horizontalOffset;
    private int verticalOffset;
    private Color textColor;
    private BitmapFont fontCache;
    private ResourceManager.ResourceIdentifier fontAccessor;
    private LayoutManager manager;

    public Style(int horizontalOffset, int verticalOffset, Color textColor, BitmapFont font, LayoutManager manager) {
        this.horizontalOffset = horizontalOffset;
        this.verticalOffset = verticalOffset;
        this.textColor = textColor;
        this.fontCache = font;
        this.manager = manager;
    }

    public Style(int horizontalOffset, int verticalOffset, Color textColor, ResourceManager.ResourceIdentifier font, LayoutManager manager) {
        this.horizontalOffset = horizontalOffset;
        this.verticalOffset = verticalOffset;
        this.textColor = textColor;
        this.fontAccessor = font;
        this.manager = manager;
    }

    public Style(int horizontalOffset, int verticalOffset, Color textColor, LayoutManager manager) {
        this.horizontalOffset = horizontalOffset;
        this.verticalOffset = verticalOffset;
        this.textColor = textColor;
        this.fontAccessor = null;
        this.fontCache = null;
        this.manager = manager;
    }

    /**
     * @return the horizontalOffset
     */
    public int getXOffset() {
        return horizontalOffset;
    }

    /**
     * @return the verticalOffset
     */
    public int getYOffset() {
        return verticalOffset;
    }

    /**
     * @return the textColor
     */
    public Color getTextColor() {
        return textColor;
    }

    /**
     * @return the font
     */
    public BitmapFont getFont() {
        if (fontCache == null) {
            if (fontAccessor == null) {
                return View.DEFAULT_FONT;
            }
            fontCache = ResourceManager.get(BitmapFont.class, fontAccessor);
            fontAccessor = null;
        }
        return fontCache;
    }

    public Rectangle createIdealBorderRectangle(Component forComponent) {
        if (getManager() != null) {
            return getManager().createIdealBorderRectangle(this, forComponent);
        } else {
            return forComponent.getBorder();
        }
    }

    /**
     * @return the manager
     */
    public LayoutManager getManager() {
        return manager;
    }

    public void setHorizontalOffset(int horizontalOffset) {
        this.horizontalOffset = horizontalOffset;
    }

    public void setVerticalOffset(int verticalOffset) {
        this.verticalOffset = verticalOffset;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    public void setFont(ResourceManager.ResourceIdentifier fontAccessor) {
        this.fontAccessor = fontAccessor;
        fontCache = null;
    }

    public void setManager(LayoutManager manager) {
        this.manager = manager;
    }

    public interface LayoutManager {

        public static final LayoutManager LINE_STACKING_LAYOUT_MANAGER = new LayoutManager() {
            @Override
            public Rectangle createIdealBorderRectangle(Style withStyle, Component forComponent) {
                int x = withStyle.getXOffset();
                int width = (int) (forComponent.getParent().getBorder().getWidth(forComponent.getDimension()) - (2 * withStyle.getXOffset()));
                int y = withStyle.getYOffset();
                for (Component alreadyInComponent : forComponent.getParent().getComponents()) {
                    if (!alreadyInComponent.equals(forComponent)) {
                        int yAccordingToAlreadyIn = (int) (alreadyInComponent.getBorder().getY(forComponent.getDimension()) + alreadyInComponent.getBorder().getHeight(forComponent.getDimension()));
                        y = Math.max(y, yAccordingToAlreadyIn);
                    }
                }
                int height = forComponent.getPreferredHeight();
                return new Rectangle.RelativeRectangle(forComponent.getDimension(), x, y, width, height);
            }
        };

        public static final LayoutManager COLUMN_LAYOUT_MANAGER = new LayoutManager() {

            @Override
            public Rectangle createIdealBorderRectangle(Style withStyle, Component forComponent) {
                int x = withStyle.getXOffset();
                int y = withStyle.getYOffset();
                int width = forComponent.getPreferredWidth();
                int height = forComponent.getPreferredHeight();

                for (Component alreadyInComponent : forComponent.getParent().getComponents()) {
                    if (!alreadyInComponent.equals(forComponent)) {
                        int yAccordingToAlreadyIn = (int) (alreadyInComponent.getBorder().getY(forComponent.getDimension()) + alreadyInComponent.getBorder().getHeight(forComponent.getDimension()) + withStyle.getYOffset());
                        y = Math.max(y, yAccordingToAlreadyIn);
                    }
                }
                return new Rectangle.RelativeRectangle(forComponent.getDimension(), x, y, width, height);
            }
        };

        public static final LayoutManager ROW_LAYOUT_MANAGER = new LayoutManager() {

            @Override
            public Rectangle createIdealBorderRectangle(Style withStyle, Component forComponent) {
                int x = withStyle.getXOffset();
                int y = withStyle.getYOffset();
                int width = forComponent.getPreferredWidth();
                int height = forComponent.getPreferredHeight();

                for (Component alreadyInComponent : forComponent.getParent().getComponents()) {
                    if (!alreadyInComponent.equals(forComponent)) {
                        int xAccordingToAlreadyIn = (int) (alreadyInComponent.getBorder().getX(forComponent.getDimension()) + alreadyInComponent.getBorder().getWidth(forComponent.getDimension()) + withStyle.getXOffset());
                        x = Math.max(x, xAccordingToAlreadyIn);
                    }
                }
                return new Rectangle.RelativeRectangle(forComponent.getDimension(), x, y, width, height);
            }
        };

        public static final LayoutManager FULL_STRETCH_LAYOUT_MANAGER = new LayoutManager() {
            @Override
            public Rectangle createIdealBorderRectangle(Style withStyle, Component forComponent) {
                return forComponent.getParent().getBorder();
            }
        };

        public static final LayoutManager KEEP_INTACT_LAYOUT_MANAGER = new LayoutManager() {
            @Override
            public Rectangle createIdealBorderRectangle(Style withStyle, Component forComponent) {
                return forComponent.getBorder();
            }
        };

        public Rectangle createIdealBorderRectangle(Style withStyle, Component forComponent);
    }

    public static LayoutManager factoryCUSTOM_ALIGMENT_MANAGER(final float x, final float y) {
        return new LayoutManager() {

            @Override
            public Rectangle createIdealBorderRectangle(Style withStyle, Component forComponent) {
                int width = (int) forComponent.getBorder().getWidth(forComponent.getDimension());
                int height = (int) forComponent.getBorder().getHeight(forComponent.getDimension());
                return new Rectangle.RelativeRectangle(forComponent.getDimension(), (int) (forComponent.getParent().getBorder().getWidth(forComponent.getDimension()) * x - width / 2), (int) (forComponent.getParent().getBorder().getHeight(forComponent.getDimension()) * y - height / 2), width, height);
            }
        };
    }
}
