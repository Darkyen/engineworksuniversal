package darkyenus.engineworksuniversals.geometry;

/**
 * Defines a rectangular area with some utility classes.
 * <p/>
 * All the input and output is in absolute coordinates.
 * <p/>
 * Private property.
 * User: Darkyen
 * Date: 4/5/13
 * Time: 9:01 AM
 */
@SuppressWarnings("UnusedDeclaration")
public interface Rectangle {

    public boolean contains(Dimension view, float x, float y);

    public void setWidth(Dimension view, float width);

    public void setHeight(Dimension view, float height);

    public float getWidth(Dimension view);

    public float getHeight(Dimension view);

    public float getX(Dimension view);

    public float getY(Dimension view);

    public void setX(Dimension view, float x);

    public void setY(Dimension view, float y);

    public void setXY(Dimension view, float x, float y);

    public Rectangle copy();

    @SuppressWarnings("UnusedDeclaration")
    public static class AbsoluteRectangle implements Rectangle {
        private float x;
        private float y;
        private float width;
        private float height;

        public AbsoluteRectangle(float x, float y, float width, float height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        public AbsoluteRectangle() {
            this.x = 0;
            this.y = 0;
            this.width = 0;
            this.height = 0;
        }

        public AbsoluteRectangle(Point point, float width, float height) {
            x = point.getX();
            y = point.getY();
            this.width = width;
            this.height = height;
        }

        public AbsoluteRectangle(Dimension dimension, Rectangle rectangle) {
            this(rectangle.getX(dimension), rectangle.getY(dimension), rectangle.getWidth(dimension), rectangle.getHeight(dimension));
        }

        @Override
        public boolean contains(Dimension view, float x, float y) {
            if (x >= this.x && x <= this.x + this.width) {
                if (y >= this.y && y <= this.y + this.height) {
                    return true;
                }
            }
            return false;
        }

        public void setWidth(Dimension view, float width) {
            this.width = width;
        }

        public void setHeight(Dimension view, float height) {
            this.height = height;
        }

        public float getWidth(Dimension view) {
            return width;
        }

        public float getHeight(Dimension view) {
            return height;
        }

        public float getX(Dimension view) {
            return x;
        }

        public float getY(Dimension view) {
            return y;
        }

        public void setX(Dimension view, float x) {
            this.x = x;
        }

        public void setY(Dimension view, float y) {
            this.y = y;
        }

        public void setXY(Dimension view, float x, float y) {
            this.x = x;
            this.y = y;
        }

        public Rectangle copy() {
            return new AbsoluteRectangle(x, y, width, height);
        }
    }

    public static class RelativeRectangle implements Rectangle {

        private float x;
        private float y;
        private float width;
        private float height;

        public RelativeRectangle(Dimension dimension, float x, float y, float width, float height) {
            this.x = x / dimension.getWidth();
            this.y = y / dimension.getHeight();
            this.width = width / dimension.getWidth();
            this.height = height / dimension.getHeight();
        }

        public RelativeRectangle(Dimension dimension, Rectangle rectangle) {
            this(dimension, rectangle.getX(dimension), rectangle.getY(dimension), rectangle.getWidth(dimension), rectangle.getHeight(dimension));
        }

        protected RelativeRectangle(float x, float y, float width, float height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        @Override
        public boolean contains(Dimension view, float x, float y) {
            float relativeX = x / view.getWidth();
            float relativeY = y / view.getHeight();

            if (relativeX >= this.x && relativeX <= this.x + this.width) {
                if (relativeY >= this.y && relativeY <= this.y + this.height) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public void setWidth(Dimension view, float width) {
            this.width = width / view.getWidth();
        }

        @Override
        public void setHeight(Dimension view, float height) {
            this.height = height / view.getHeight();
        }

        @Override
        public float getWidth(Dimension view) {
            return width * view.getWidth();
        }

        @Override
        public float getHeight(Dimension view) {
            return height * view.getHeight();
        }

        @Override
        public float getX(Dimension view) {
            return x * view.getWidth();
        }

        @Override
        public float getY(Dimension view) {
            return y * view.getHeight();
        }

        @Override
        public void setX(Dimension view, float x) {
            this.x = x / view.getWidth();
        }

        @Override
        public void setY(Dimension view, float y) {
            this.y = y / view.getHeight();
        }

        @Override
        public void setXY(Dimension view, float x, float y) {
            setX(view, x);
            setY(view, y);
        }

        public Rectangle copy() {
            return new RelativeRectangle(x, y, width, height);
        }
    }
}
