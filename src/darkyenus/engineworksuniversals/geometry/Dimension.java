package darkyenus.engineworksuniversals.geometry;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/1/13
 * Time: 12:29 PM
 */
public interface Dimension {

    public int getWidth();

    public int getHeight();
}
