package darkyenus.engineworksuniversals.geometry;

/**
 * Private property.
 * User: Darkyen
 * Date: 4/6/13
 * Time: 5:07 PM
 */
public class Point {
    private float x;
    private float y;

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Point() {
        this.x = 0;
        this.y = 0;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void clear() {
        x = 0;
        y = 0;
    }

}
