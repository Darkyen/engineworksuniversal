package darkyenus.engineworksuniversals.logging;

import darkyenus.utils.math.Averager;
import darkyenus.utils.math.StaticAverager;

import java.util.HashMap;
import java.util.Map;

/**
 * Private property.
 * User: Darkyen
 * Date: 6/7/13
 * Time: 8:19 PM
 */
public class TimeLogger {

    private static boolean enabled = true;
    private static HashMap<String,Averager> averagers = new HashMap<String, Averager>();
    private static long lastLog = -1;

    public static void log(String part){
        if(lastLog == -1){
            lastLog = System.currentTimeMillis();
        }else{
            long delta = System.currentTimeMillis() - lastLog;
            lastLog = System.currentTimeMillis();
            Averager averager = averagers.get(part);
            if(averager == null){
                averager = new StaticAverager();
                averagers.put(part,averager);
            }
            averager.addValue(delta);
        }
    }

    public static String getLog(){
        double total = 0;
        for(Averager averager:averagers.values()){
            total += averager.getAverage();
        }

        StringBuilder result = new StringBuilder();
        for(Map.Entry<String,Averager> entry:averagers.entrySet()){
            result.append(entry.getKey()).append(": ").append((int)entry.getValue().getAverage()).append(" - ").append((int)((entry.getValue().getAverage()/total)*100.0)).append(" %\n");
        }
        return result.toString();
    }
}
