package darkyenus.engineworksuniversals.logging;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/29/13
 * Time: 5:01 PM
 */
public class SimpleLogger implements AbstractLogger {

    private int importance;
    private PrintStream output;
    private Date date = new Date();
    private DateFormat format = DateFormat.getTimeInstance(DateFormat.SHORT);

    public SimpleLogger(Importance importance, PrintStream output) {
        this.importance = importance.ordinal();
        this.output = output;
    }

    public SimpleLogger(Importance importance) {
        this.importance = importance.ordinal();
        output = System.out;
    }

    @Override
    public void log(Importance importance, String message, Object... additionalInfo) {
        if (importance.ordinal() >= this.importance) {
            date.setTime(System.currentTimeMillis());
            output.println(format.format(date) + " " + importance + ": " + message + (additionalInfo.length != 0 ? " : " + Arrays.toString(additionalInfo) : ""));
        }
    }

    @Override
    public void log(Importance importance, String message, Throwable error, Object... additionalInfo) {
        if (importance.ordinal() >= this.importance) {
            date.setTime(System.currentTimeMillis());
            output.println(format.format(date) + " " + importance + ": " + message + " : " + error.toString() + (additionalInfo.length != 0 ? " : " + Arrays.toString(additionalInfo) : ""));
            error.printStackTrace(output);
        }
    }
}
