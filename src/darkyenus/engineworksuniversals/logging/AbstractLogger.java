package darkyenus.engineworksuniversals.logging;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/29/13
 * Time: 4:59 PM
 */
public interface AbstractLogger {

    public void log(Importance importance, String message, Object... additionalInfo);

    public void log(Importance importance, String message, Throwable error, Object... additionalInfo);

    public enum Importance {
        DEBUG,
        INFO,
        WARNING,
        CRITICAL
    }
}
