package darkyenus.engineworksuniversals.tween;

/**
 * Used for tweening.
 * <p/>
 * Private property.
 * User: Darkyen
 * Date: 4/4/13
 * Time: 8:45 PM
 */
public interface Tween {

    /**
     * Returns inverted f to the power of two, so it seems it is slowing down at the end.
     */
    public static final Tween INVERTED_EXPONENCIAL = new Tween() {
        @Override
        public float get(float f) {
            return (float) (1 - Math.pow(f - 1, 2));
        }
    };

    /**
     * Returns inverted f to the power of four, so it seems it is slowing down at the end.
     */
    public static final Tween INVERTED_FOURTH_EXPONENCIAL = new Tween() {
        @Override
        public float get(float f) {
            return (float) (1 - Math.pow(f - 1, 4));
        }
    };

    /**
     * Returns inverted f to the power of three, so it seems it is slowing down at the end.
     */
    public static final Tween INVERTED_THIRD_EXPONENCIAL = new Tween() {
        @Override
        public float get(float f) {
            return (float) (1 - Math.pow(f - 1, 3));
        }
    };

    /**
     * Returns f
     */
    public static final Tween LINEAR = new Tween() {
        @Override
        public float get(float f) {
            return f;
        }
    };

    /**
     * Returns f to the power of 2
     */
    public static final Tween EXPONENCIAL = new Tween() {
        @Override
        public float get(float f) {
            return f * f;
        }
    };

    /**
     * Returns f to the power of 1.5
     */
    public static final Tween HALF_EXPONENCIAL = new Tween() {
        @Override
        public float get(float f) {
            return (float) Math.pow(f, 1.5);
        }
    };

    /**
     * Returns f to the power of 3
     */
    public static final Tween THIRD_EXPONENCIAL = new Tween() {
        @Override
        public float get(float f) {
            return f * f * f;
        }
    };

    /**
     * Returns f to the power of 4
     */
    public static final Tween FOURTH_EXPONENCIAL = new Tween() {
        @Override
        public float get(float f) {
            return f * f * f * f;
        }
    };

    /**
     * Returns always 1.
     */
    public static final Tween FINISHED = new Tween() {
        @Override
        public float get(float f) {
            return 1;
        }
    };

    /**
     * Get tween.
     * Supply argument from 0 to 1 (both inclusive) to get number in same range (unless specified otherwise in implementation declaration)
     * Should return 0 for f = 0 and 1 for f = 1. Other results are matter of implementation.
     *
     * @param f number to tween
     * @return tweened number f
     */
    public float get(float f);
}
